$mslVTbasic	Basic
$mslVTadvanced	Advanced
$mslVThelptutorials	Help & Tutorials
$mslVTcompatibility	Compatibility
$mslVTgeneral	General
$mslVTturntoashO	Turn To Ash
$mslVTdialspeechO	Speech Gains
$mslVTdeathsceneO	Animated Transformation
$mslVTnobitecontrolsO	Feeding Restrictions
$mslVTdialdifficultyO	Dialogue Difficulty
$mslVTfeeddelayO	Feed Animation Sync
$mslVTsundamage	Sun Damage
$mslVTsundamageO	Base Sun Damage
$mslVTsunalertO	Sunrise Warning
$mslVTsundawnO	Sunrise Time
$mslVTsunduskO	Sunset Time
$mslVTbloodquality	Blood Quality
$mslVTbloodratioO	Blood Ratio
$mslVTbloodaddictionO	Addiction Threshold
$mslVTbloodqualityO	Blood Quality Mode
$mslVTfull	Full
$mslVTpartial	Partial
$mslVTdisabled	Disabled
$mslVTbloodpercentO	Preference Gain Ratio
$mslVTfunctions	Functions
$mslVTconvert	Convert
$mslVTupdate	Update
$mslVTuninstall	Uninstall
$mslVTappearance	Appearance
$mslVTvisage	Visage
$mslVTvampire	Vampire
$mslVTnormal	Normal
$mslVTkillessentialO	Allow Killing Essentials
$mslVTcloakoffO	Emergency Brawl Fix
$mslVTageing	Ageing
$mslVTagereq1O	Child Stage
$mslVTagereq2O	Neophyte Stage
$mslVTagereq3O	Junior Stage
$mslVTagereq4O	Bloodkin Stage
$mslVTageingmultO	Rate Of Ageing
$mslVTupdateageO	Update Age
$mslVTexperience	Experience
$mslVTvlreqbaseO	VL Base
$mslVTvlreqincrementO	VL Increment
$mslVTexpmultO	Experience Multiplier
$mslVThotkeys	Hotkeys
$mslVTkeyfeedO	Feed
$mslVTkeystatusO	Check Bloodpool
$mslVTkeypsmenuO	Unlock Abilities
$mslVTturntoashHL	If enabled vampires will turn to ash when they die.\nDefault: Enabled
$mslVTdialspeechHL	Set whether successfully passing feeding dialogue checks increases Speech skill.\nDefault: Enabled
$mslVTdeathsceneHL	Enable or disable death animations during player's transformation scene.\nDisable if the animations are causing any problems.\nDefault: Enabled
$mslVTnobitecontrolHL	Prevent player from biting if controls are disabled or while wearing full helmets or gags.\nEnabling it helps immersion and compatibility with mods like Sanguine Debauchery.\nDefault: Enabled
$mslVTdialdifficultyHL	Adjust the overall difficulty of Speech checks for dialogue feeding.\nHigher values make passing the checks harder.\nDefault: 0
$mslVTfeeddelayHL	Adjust how long script should wait for the animation to finish before victim dies  or loses consciousness.\nDeath or paralysis should happen exactly at the end of feeding animation.\nDefault: 0.35
$mslVTsundamageHL	Set the base sun damage per second before environmental factors are taken into account.\nSetting to 0  disables sun damage completely.\nDefault: 10
$mslVTsunalertHL	Set when player receives a warning that sunrise is coming soon. Set to 0 to disable.\nThese values equal hours on a 24 hour clock. Use fractions of 1.0 to specify minutes (for example 6:30 = 6.50)\nDefault: 5.50
$mslVTsundawnHL	Set when sun damage begins. It should occur near sunrise.\nThese values equal hours on a 24 hour clock. Use fractions of 1.0 to specify minutes (for example 6:30 = 6.50)\nDefault: 6.00, Vanilla: 5.00
$mslVTsunduskHL	Set when sun damage ends. It should occur near sunset.\nThese values equal hours on a 24 hour clock. Use fractions of 1.0 to specify minutes (for example 6:30 = 6.50)\nDefault: 19.00, Vanilla: 19.00
$mslVTbloodratioHL	Set the base of how many blood points you gain per health point stolen from your victim\nNew default: 16, Old default: 8, Original  default: 4
$mslVTbloodaddictionHL	Preference level needed to trigger addiction after feeding from a given person.\nDefault: 30%
$mslVTbloodqualityHL	Disabled: Disables all Blood Quality features.\nPartial: Only enables blood preference.\nFull: Enables both blood preference and addiction.
$mslVTbloodpercentHL	Drinking 100% of a person's blood equals % preference increase for that person.\nDefault: 5%
$mslVTdebconvertHL	Run this to convert a vanilla vampire into Vampiric Thirst vampire or to turn a mortal character into a vampire.
$mslVTdebupdateHL	Use if updating VT to a newer version.\nThis is also your panic button: use whenever something isn't working like it should!
$mslVTdebuninstallHL	Use this to uninstall Vampiric Thirst and remove whatever traces it can from your game.\nWarning! To do this  it must cure player's vampirism too!
$mslVTvisageHL	Choose whether you want to have the feral look of a vampire or be indistinguishable from mortals.\nIt will take effect immediately if player is a vampire or wait for player to become a vampire if not.\nThis choice is aesthetic and has no effect on gameplay.
$mslVTdebexitmenu	Selected procedure will start once you exit menu.
$mslVTkillessentialHL	Checking this will allow draining essential characters to death.\nONLY USE IF YOU KNOW WHAT YOU'RE DOING!\nDefault: Disabled
$mslVTcloakoffHL	Enable this if experiencing issues with brawls\nUSE ONLY FOR THE DURATION OF BRAWL AND DISABLE AFTERWARDS!!!\nDefault: Disabled
$mslVTagereq1HL	Set how many days since becoming a vampire must pass before advancing to the Child stage.\nDefault: 7
$mslVTagereq2HL	Set how many days since becoming a vampire must pass before advancing to the Neophyte stage.\nDefault: 21
$mslVTagereq3HL	Set how many days since becoming a vampire must pass before advancing to the Junior stage.\nDefault: 90
$mslVTagereq4HL	Set how many days since becoming a vampire must pass before advancing to the Bloodkin stage.\nDefault: 270
$mslVTageingmultHL	Set the overall scale of how fast vampire ageing progresses.\nHigher values mean faster ageing.\nDefault: 1.0
$mslVTupdateageHL	Click this to update your age if you modified any of the above values.
$mslVTvlreqbaseHL	Requirements for advancing through Vampire Lord perk tree are calculated according to:\nNext perk = Base + (Total number of perks earned * Increment)\nDefault: 14, Vanilla: 5
$mslVTvlreqincrementHL	Requirements for advancing through Vampire Lord perk tree are calculated according to:\nNext perk = Base + (Total number of perks earned * Increment)\nDefault: 7, Vanilla: 2
$mslVTexpmultHL	Multiplier of all Blood Experience gained by feeding and using abilities.\nDefault: 0.7
$mslVTkeyfeedHL	The hotkey used to initiate contextual feeding interactions.\nDefault: V
$mslVTkeystatusHL	The hotkey to display how hungry player character is.\nDefault: B
$mslVTkeypsmenuHL	Pressing this hotkey displays a menu of powers you can buy or upgrade.\nDefault: H
$mslVT0days	{0} days
$mslVTmodinfo	Mod Info
$mslVTinfo1Q	What is Vampiric Thirst?
$mslVTinfo1A	Vampiric Thirst is a complete vampirism overhaul that has only one purpose: make vampirism feel different. It is not supposed be better or bloodier than vanilla system but to create a fresh and fun experience while staying faithful to the world of Nirn. Vampiric Thirst wants you to be involved and aware of your vamprism in an immersive way, to decide how you want to hunt for blood, what kind of abilities to develop and how to use them.
$mslVTinfo2Q	What is Vampiric Thirst not?
$mslVTinfo2A	Vampiric Thirst is not a mod that gives you anything for free, it is not a mod that gives you moar powah and l33t st47z and it is not a mod that forgives and forgets your mistakes. It starts you with nothing and expects you to work your way up from there through careful planning and hard work. But it doesn't leave your efforts unrewarded! As you play your vampire will develop and learn to control first her monstrous nature and eventually the world and people around her.
$mslVTinfo3Q	What do I need to play?
$mslVTinfo3A	The mod requires the latest versions of Skyrim, Dawnguard and SKSE as of the date of its release. You also need to have SkyUI with MCM (Mod Configuration Menu) support. And because it's a very complex mod that makes huge changes to the game and may leave traces in your savegames even after uninstalling you also need to have basic knowledge about the consequences of modding your game. If you are not doing it responsibly you probably shouldn't be doing it at all.
$mslVTinfo4Q	What should I do if I encounter a problem?
$mslVTinfo4A	The first step is to see if the problem is already answered in the FAQ below. If it's not you should run the Update function. Go to Basic options and choose Update from the list on the right. If the problem persists after it's finished please check if you have any other mods (even inactive!) that may be conflicting with Vampiric Thirst. If it's not that either you may report the bug but PLEASE provide a detailed description of it and the circumstances when you encountered it.
$mslVTinfo5Q	Who made this mod?
$mslVTinfo5A	Vampiric Thirst was made and published by Miss Leeches. She is an alien princess from a galaxy far away who currently lives on her huge ship orbiting the earth. You can't see it because of its advanced cloaking technology and because you don't keep your head in the clouds as much as you should. If you *can* see it, you are amazing and she loves you! But if you are being rude, are making unreasonable requests or aren't following instructions she finds where you are and launches orbital strikes.
$mslVTfaq	FAQ
$mslVTfaq1Q	My blood is deteriorating so fast! Why?
$mslVTfaq1A	All vampiric powers have a cost and some of them cost more than others. Activable powers (Seduction, Corpse Marionette, Mistform and so on) drain some blood when you activate them and toggleable powers (like Deadly Strength, Extended Perception, Embrace of Shadows and so on) drain it continuously while they're on. Also sleeping to pass the time instead of waiting slows down your natural consumption.
$mslVTfaq2Q	I keep getting damaged by sun even in shade!
$mslVTfaq2A	Shadows don't remove sun damage but they can reduce it a lot. The only ways to avoid sun damage during the day is find shelter inside a building or fill blood pool above 90%
$mslVTfaq3Q	Some of my powers are so weak!
$mslVTfaq3A	They are weak at first but they grow in power as you get older. For example older vampires can raise more powerful corpses, mesmerise higher level people, summon more gargoyles, use Celerity, Mistform and Ferocious Surge for longer and so on.
$mslVTfaq4Q	My Vampire Lord doesn't have starting spells!
$mslVTfaq4A	That's intentional. Before you can access those spells you need to unlock them first. To do that use the "Vampiric Abilities" lesser power in your magic menu or by pressing your Unlock Abilities hotkey (default: H) once you have enough experience (which you can gain by feeding and using your powers).
$mslVTfaq5Q	Will you ever allow turning NPCs into vampires?
$mslVTfaq5A	No I'm sorry. There are many reasons: it is too difficult to make it work right and without issues, it is too invasive and can irrevocably damage your savegame and I'm simply not adding any big features anymore. But if you really want to turn your followers into vampires Amazing Follower Tweaks already has that functionality and is fully compatible with Vampiric Thirst!
$mslVTfaq6Q	Can I use VT with another vampirism mod?
$mslVTfaq6A	Probably not. Aesthetic mods (like removing sunken cheeks or changing the looks of vampire lords) are compatible but if a mod changes any aspect of vampire gameplay it will conflict. Nexus mod description has a full list of vanilla assets Vampiric Thirst changes, if another mod changes them too they aren't going to play nice together.
$mslVTfaq7Q	After becoming a vampire everything goes weird!
$mslVTfaq7A	It's either because of a mod conflict or because you played the game with the wrong PlayerVampireQuestScript.pex inside your Data/Scripts folder in which case you should remove it. In either case you should revert to a save before becoming a vampire and run the Update function. And then run it again after transformation is done just to be safe.
$mslVTfaq8Q	Do I always need to read stuff before using mods?
$mslVTfaq8A	All cool people do! Please be a cool person too!
$mslVTtutorials	Tutorials
$mslVTtut1Q	Recommendations
$mslVTtut1A	Before you start your adventure with Vampiric Thirst I recommend setting your Timescale to a lower value than default. Vampiric Thirst is smart and adjusts to whatever timescale you have but you won't have lots of night time and your hunger will be unforgiving at the vanilla value (20). The mod was mostly tested on Timescales 8 and 12 and the experience was way better! To set your timescale open console and type: set Timescale to X (where X put the desired value).
$mslVTtut2Q	Becoming a vampire
$mslVTtut2A	There are a few ways to become a vampire: you can get infected with Sanguinare Vampiris while fighting vampires or while progressing through the story of Dawnguard. It can be slow and leave you time to change your mind or happen suddenly. In either case it results in your death. But once the sun is gone from the sky you will open your eyes and walk again as one of the undead.
$mslVTtut3Q	Thirst for blood
$mslVTtut3A	You will most likely awaken with only the remnants of your old mortal blood to fuel you and it won't last for long. As you grow hungry you will have more difficulty concentrating on casting spells and your monstrous nature will start shining through the facade of normalcy and alert people and creatures around you. If you let yourself go hungry your acute senses may start playing tricks on you and common animals will become startled by your predatory presence.
$mslVTtut4Q	Feeding
$mslVTtut4A	There are many ways to slake that thirst but how many mortals are you willing to hurt or kill for a moment of respite? Drinking a person's blood may leave her or him weakened for days and even Restoration magic won't rush the recovery. If you keep feeding on a single person without giving her or him enough time to recover you may soon have bloodless corpse on your conscience. Use your Feed power or hotkey to initiate most feeding interactions or talk to someone for subtler feeding options.
$mslVTtut5Q	Feeding: stealth
$mslVTtut5A	Thanks to their agility and predatory instincts many vampires find it easy to hide and stalk their prey from shadows even if they weren't very stealthy in life. Like their cousins from Cyrodiil Skyrim vampires are able to feed on sleeping victims without alerting them. They can also sneak up on awake prey and quickly drain it dry (if weapon is drawn) or drink just enough to leave it unconscious.
$mslVTtut6Q	Feeding: seduction
$mslVTtut6A	A vampire confident in her or his sexuality may try using it to open up hearts and veins. Seduce your chosen vessel to feed under the pretence of intimate caresses. If you succeed you may expect that person won't forget it soon and may wish to experience that unique mysterious sensation again. But charm isn't all there is to it! Some races are more lustful than others, dressing up pretty will help, trust goes a long way and so on. If you fail you must raise your Speech skill to try again.
$mslVTtut7Q	Feeding: reveal
$mslVTtut7A	You can also forget deception and reveal your vampirism hoping that your confidant trusts you as much as you do. The loyalty of a willing vessel is priceless but the truth may also be difficult to accept. Skyrim people can be stubborn and distrustful and they can decide to confront their fears with violence. Trust is essential to success, sharing allegiance is helpful too but avoid negative impressions. If you fail you must raise your Speech skill to try again.
$mslVTtut8Q	Feeding: intimidation
$mslVTtut8A	You could take what you want by force... or you could just show that you could if you wanted to. Intimidating your victim into submission will let you feed without causing more harm or chaos than necessary. However it will never win you lasting loyalty and in fact may make subsequent persuasion attempts more difficult. Fail and you better have more than threats. Being physically imposing, armed with fearsome weapons and armour, belonging to certain races and so on can be helpful.
$mslVTtut9Q	Feeding: combat
$mslVTtut9A	If forced into close combat a vampire can attempt to grapple the opponent and finish her or him off with a deadly bite (if weapon is drawn) or take her or him out of the fight for a while (if weapon is sheathed). If at first you don't succeed and are pushed away try to wear down the opponent's health and stamina before trying again. Enemies skilled in blocking are more difficult to grapple but if distracted by someone else or attacked from behind they will be easy prey.
$mslVTtut10Q	Feeding: magic
$mslVTtut10A	Skyrim vampires have a natural talent for illusions and blood magic. They can learn to drain lifeforce directly from the victim, hypnotise with a mesmerising gaze, paralyse the prey to feed on it safely or use magic to tip the scales in their favour if hunting in other ways. Relying on magic requires investment and specialisation but it can make undeath a little easier.
$mslVTtut11Q	Feeding: corpses
$mslVTtut11A	It may be disgusting but sometimes necessity is the mother of invention and sometimes she's a freshly murdered peasant who still has enough warm blood left to keep your thirst at bay for a moment longer. Fledgling vampires and people who were never cut out for this kind of life in the first place may be forced to resort to this a lot until they develop their hunting skills.
$mslVTtut12Q	Feeding: bottled blood
$mslVTtut12A	Blood in storage will quickly lose its unique flavour but will still provide a little nourishment. It's not the yummiest choice of sustenance but vampires going on long trips into remote regions disconnected from civilisation should remember to stock up on it just in case. If you have empty wine bottles on hand you can fill them with the blood of your victim instead of feeding the normal way. The bottles are reusable.
$mslVTtut13Q	Preference, quality, addiction
$mslVTtut13A	Feeding repeatedly on the same person will gradually increase your preference for her or his blood making it more nourishing each time. If the preference is high enough sampling the blood will trigger a short lasting bout of addictive ecstasy. During it you will gain certain benefits depending on the vessel's traits like race, gender, magical power and so on but it will also cause any blood from less preferred sources to be only half as nourishing for the duration.
$mslVTtut14Q	Sunlight
$mslVTtut14A	Before sunscreen was invented sunlight was deadly to vampires in Tamriel... And since it won't be invented until M'aiq comes up with the formula 669 years from now it is still deadly to vampires. Hunger makes it more acute while staying in shade, cloudy weather and being underwater help reduce it. Being fully fed (blood pool above 90%) makes you immune to sun damage but even then daylight makes you sluggish and vulnerable. Sunbathing is a bad idea too.
$mslVTtut15Q	Blood Experience
$mslVTtut15A	Drinking blood and using powers slowly teaches you control over your vampiric state and allows you to gradually unlock its hidden potential. When you reach enough experience to gain a point to spend on new abilities a sound and a message will notify you. Use your Vampiric Abilities lesser power or your Unlock Abilities hotkey to access a selection of available powers. Some powers may cost more than 1 point.
$mslVTtut16Q	Bloodline Experience
$mslVTtut16A	When you join Harkon's bloodline and become a Vampire Lord additional abilities will become available to you. You can access them by entering your inventory while in Vampire Lord form and any powers you buy will also be available or have a close equivalent in your human form and the other way around too. You gain bloodline experience by killing people and creatures with Vampire Lord bite power attack or Vampiric Drain spell if you have it unlocked.
$mslVTtut17Q	Sleeping
$mslVTtut17A	Vampires don't need to sleep but many decide to do it to pass the time and conserve their strength during the day. Sleep in a bed or your comfy coffin to slow down your natural blood deterioration. Sweet dreams!
$mslVTtut18Q	Ageing
$mslVTtut18A	As you survive you will slowly grow more accustomed to your undead state. Legendary ancients have mastered their forms over millennia but even in a few months you can make impressive progress. As you age your stamina regeneration, speed, stealth and illusion affinities, blood pool and blood deterioration rate will all gradually improve. By the time you reach the Bloodkin stage you will have learned to control your hunger and will be ready to face eternity.
$mslVTrnd	Realistic Needs and Diseases
$mslVTrndbottlesO	Patch Empty Bottles
$mslVTrndbottlesHL	Allow VT to detect and use empty bottles from Realistic Needs and Diseases.
$mslVThealmodeO	Healing Resistance
$mslVThealmodeHL	Choose how ineffective conventional (not necromantic) healing magic is on vampires.\nThe percentage will be subtracted from healing spells, enchantments and potions.\nVampiric Thirst default: 50%. Requiem default: 100% (not recommended)
$mslVTregenmodeO	Regeneration Resistance
$mslVTregenmodeHL	Choose how ineffective conventional (not necromantic) health regeneration magic is on vampires.\nThe percentage will be subtracted from health regeneration spells, enchantments and potions.\nVampiric Thirst default: 50%
$mslVT100percent	100%
$mslVT75percent	75%
$mslVT50percent	50%
$mslVT25percent	25%
$mslVT0percent	0%