Scriptname MSLVTVampire hidden  

import mslVT_Utils
import JMap
import Logger

;/
	DetNatMod
	DetNatMult
	DetAct
	DetActMult: 1
	Blood
	BloodMax
	BloodMaxMod


	mapping:

	BloodMax: JDB.ageing[N].BloodMax
	DetNat: JDB.ageing[N].DetNat
/;

int function create(Actor vamp) global
	int e = JFormDB.findEntry("mslVT", vamp)
	if !e
		e = JMap.object()
		JFormDB.setEntry("mslVT", vamp, e)

		setFlt(e, "BloodMax", 1000)
		setFlt(e, "Blood", 900)

		setFlt(e, "DetActMult", 1)
		setFlt(e, "DetNatMult", 1)
		setFlt(e, "DetAct", 0)

		Debug.Notification("created MSLVTVampire entry for " + vamp)
	Endif
	return e
endfunction

int function get(Actor vamp) global
	return create(vamp)
endfunction

;;;; Ageing

int function getAge(Actor vamp) global
	return getInt(get(vamp), "Age")
endfunction

function setAge(Actor vamp, int Age) global

	int jNewAge = MSLVTConfig.solveObj(".ageing["+Age+"]")
	if !jNewAge
		return
	endif

	int e = get(vamp)
	int currAge = getAge(vamp)

	;;;;;;;;;;; Health mod - 2500 blood point will add (2500 - 1000) / 10 health bonus
	float oMax = getBloodMax(vamp)
	float nMax = JValue.solveFlt(jNewAge, ".BloodMax")
	float healthMod = (nMax - oMax) / MSLVTConfig.blood2HpMult()

	;;;;;;;;;;;;;;;
	logDetail("mslVT", "setAge: transition from " + currAge + " to " + Age+". New HP mod " + healthMod)

	setInt(e, "Age", Age)

	vamp.ModAV("Health", healthMod)
	Spell sOldBuff = MSLVTConfig.solveForm(".ageing["+currAge+"].buff") as Spell
	if sOldBuff
		vamp.RemoveSpell(sOldBuff)
	endif

	Spell sNewBuff = JValue.solveForm(jNewAge,".buff") as Spell
	if sNewBuff
		vamp.AddSpell(sNewBuff, False)
	endif
endfunction

; int function getPlayer() global
; 	return get(Game.GetPlayer())
; endfunction


; function getStage(int o) global
; 	return getInt(o, "stage")
; endfunction
; WIP:
; function setStage(int o, int stage)
; 	If akTarget.HasSpell(HungerState2)
; 		HungerMod = 5.0
; 	Elseif akTarget.HasSpell(HungerState3)
; 		HungerMod = 10.0
; 	Elseif akTarget.HasSpell(HungerState4)
; 		HungerMod = 15.0
; 	Elseif akTarget.HasSpell(HungerState5)
; 		HungerMod = 20.0
; 	Endif
; endfunction

float function modParam(int entry, string k, float value) global
 	setFlt(entry, k, getFlt(entry, k) + value)
 	return getFlt(entry, k)
endfunction
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

function modDetNat(Actor vamp, float value) global
	setFlt(get(vamp), "DetNatMod", value)
endfunction

function modDetNatMult(Actor vamp, float value) global
	modParam(get(vamp), "DetNatMult", value)
endfunction

float function getDetNat(Actor vamp) global
	int e = get(vamp)
	return MSLVTConfig.solveAgeingFlt(getInt(e, "Age"), ".DetNat") + getFlt(e, "DetNatMod")
endfunction

float function getDetNatMult(Actor vamp) global
	return getFlt(get(vamp), "DetNatMult")
endfunction

;;;;;;;;;;;;;;;;;;;;;;;;;;

function modDetAct(Actor vamp, float value) global
	modParam(get(vamp), "DetAct", value)
endfunction

function modDetActMult(Actor vamp, float value) global
	modParam(get(vamp), "DetMult", value)
endfunction

float function getDetActMult(Actor vamp) global
	return getFlt(get(vamp), "DetActMult")
endfunction

float function getDetAct(Actor vamp) global
	return getFlt(get(vamp), "DetAct")
endfunction

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

float function modBlood(Actor vamp, float value) global
	return setBlood(vamp, getBlood(vamp) + value)
endfunction

; function modBloodBySpell(Actor vamp, float value) global
; 	int e = get(vamp)
; 	modParam(e, "Blood", value)

; 	;mslVTExp.Mod(CostBase/2*mslVTExpMult.GetValue())
; endfunction

float function getBlood(Actor vamp) global
	return getFlt(get(vamp), "Blood")
endfunction

float function setBlood(Actor vamp, float originValue) global
	float max = getBloodMax(vamp)
	float value = originValue
	if value > max
		value = max
	Elseif value < 0
		value = 0
	Endif
	int e = get(vamp)

	float oldValue = getFlt(e, "Blood")
	setFlt(e, "Blood", value)

	if (value as Int) != (oldValue as Int)

		;; Bind HP to blood - experiment
		float diff = originValue - oldValue
		if diff < 0.0
			vamp.DamageAV("Health", diff / MSLVTConfig.blood2HpMult())
		Else
			vamp.RestoreAV("Health", diff / MSLVTConfig.blood2HpMult())
			logDetail("mslVT", "setBlood: increase hp by " + diff / MSLVTConfig.blood2HpMult())
		endif

		if vamp == Game.GetPlayer()
			; the only way to make player's spells work =\
			GlobalVariable g_blood = MSLVTConfig.getBloodCurr()
			g_blood.SetValue(value)
		endif

		__updateHungerStage(vamp, e, value * 100.0 / max, value, oldValue)

		;MiscUtil.PrintConsole("New blood value is " + value)

		; No-one needs this yet
		; int ev = ModEvent.Create("mslVT_ChangeBlood")
		; ModEvent.PushForm(ev, vamp)
		; ModEvent.PushFloat(ev, value)
		; ModEvent.Send(ev)

		;Endif
	endif

	return value
endfunction

;;;;;;;;;;;;;;; Hunger

int function getVanillaHungerStatus(Actor vamp) global
	return MSLVTConfig.solveInt(".Hunger." + getHungerStatus(vamp) + ".vanilla", -1)
endfunction

string function getHungerStatus(Actor vamp) global
	return getStr(get(vamp), "Hunger")
endfunction

function __updateHungerStage(Actor vamp, int e, float bloodPerc, float newVal, float oldValue) global

	if newVal == 0.0 && oldValue > 0.0
		Faction f = MSLVTConfig.getNonZeroBloodFaction()
		logDetail("mslVT", "__updateHungerStage: vamp removed from NonZeroBloodFaction: " + f)
		vamp.RemoveFromFaction(f)
	Elseif newVal > 0.0 && oldValue == 0.0
		Faction f = MSLVTConfig.getNonZeroBloodFaction()
		logDetail("mslVT", "__updateHungerStage: vamp added to NonZeroBloodFaction: " + f)
		vamp.AddToFaction(f)
	endif

	string sHungerStage = getStr(e, "Hunger")
	string sNewHungerStage = __determineHungerStage(bloodPerc)
	if sHungerStage != sNewHungerStage && sNewHungerStage != ""
		Debug.Notification("Hunger: old stage " + sHungerStage + ", new stage " + sNewHungerStage)
		setStr(e, "Hunger", sNewHungerStage)
		;int jNewHungerStage = MSLVTConfig.solveObj(".Hunger." + sNewHungerStage)
		Spell sOldBuff = MSLVTConfig.solveForm(".Hunger." + sHungerStage + ".buff") as Spell
		if sOldBuff
			vamp.RemoveSpell(sOldBuff)
		endif
		Spell sNewBuff = MSLVTConfig.solveForm(".Hunger." + sNewHungerStage + ".buff") as Spell
		if sNewBuff
			vamp.AddSpell(sNewBuff, True)
		endif

		logDetail("mslVT", "__updateHungerStage: transition from " + sHungerStage + " to " + sNewHungerStage+". Buff removed "+sOldBuff+", added "+sNewBuff)
	endif
endfunction

string function __determineHungerStage(float bloodPerc) global

	int jHungerStatuses = MSLVTConfig.solveObj(".hunger")

	
	; string luaFunc = "return jc.find(vObj, function(x) return vFlt >= x.bloodPerc end)"
	; JValue.evalLuaInt_Ex(luaFunc, vFlt = bloodPerc, vObj = jHungerStatuses)

	;int jHungerStatusesCount = JArray.count(jHungerStatuses)
	string sKey = JMap.nextKey(jHungerStatuses)
	;int jNewhungerStage = 0
	while sKey
		if bloodPerc <= JValue.solveFlt(jHungerStatuses, "." + sKey + ".max") && \
			bloodPerc >= JValue.solveFlt(jHungerStatuses, "." + sKey + ".min")
			return sKey
		endif
		sKey = JMap.nextKey(jHungerStatuses, sKey)
	endwhile
	return ""
endfunction

;;;;;;;;;;;;;;;;;;;;

float function getBloodMax(Actor vamp) global
	int e = get(vamp)
	return MSLVTConfig.solveAgeingFlt(getInt(e, "Age"), ".BloodMax") + getFlt(e, "BloodMaxMod")
endfunction

function setBloodMaxMod(Actor vamp, float value) global
	setFlt(get(vamp), "BloodMaxMod", value)
endfunction

; function setBloodMax(Actor vamp, float value) global
; 	int e = get(vamp)
; 	setFlt(e, "BloodMax", value)
; endfunction
