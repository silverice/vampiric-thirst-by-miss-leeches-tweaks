Scriptname VIMM_PurpleSongEff extends activemagiceffect

;MagicEffect _base

int function createObjectIfEmpty(string path, string objecType) global
	int o = JDB.solveObj(path)
	if o
		return o
	endif

	if objecType == "JArray"
		o = JArray.object()
	elseif objecType == "JMap"
		o = JMap.object()
	elseif objecType == "JFormMap"
		o = JFormMap.object()
	elseif objecType == "JIntMap"
		o = JIntMap.object()
	endif
	JDB.solveObjSetter(path, o, true)
	return o
endfunction

Event OnEffectStart(Actor akTarget, Actor akCaster)

	;_base = self.GetBaseObject()

	Debug.Notification("VIMM_PurpleSongEff hits t: " + akTarget)

	; get or create new object of type X
	; append into array at path

	JArray.addForm(createObjectIfEmpty(".VIMM_PurpleSongEff.targets", "JArray"), akTarget)

	if JDB.solveInt(".VIMM_PurpleSongEff.alreadyWaiting") == 0
		JDB.solveIntSetter(".VIMM_PurpleSongEff.alreadyWaiting", 1, True)
		GoToState("ImMain")
	endif

EndEvent

state ImMain

	Event OnBeginState()
		lamentOfHighborne.Play(GetCasterActor())
		RegisterForSingleUpdate(GetTimeElapsed() * Utility.RandomFloat(0.25, 0.6))
	EndEvent

	Event OnUpdate()
		int targets = JDB.solveObj(".VIMM_PurpleSongEff.targets")
		Actor weak = JArray.getForm( targets, Utility.RandomInt(0, JArray.count(targets) - 1) ) as Actor
		if weak
			prey.ForceRefTo(weak)
		endif
	EndEvent

	Event OnEffectFinish(Actor akTarget, Actor akCaster)
		JDB.setObj("VIMM_PurpleSongEff", 0)
		Debug.Notification("VIMM_PurpleSongEff finished")
	EndEvent

endstate

Sound Property lamentOfHighborne Auto
ReferenceAlias Property prey Auto

