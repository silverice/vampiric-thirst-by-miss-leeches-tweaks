Scriptname MSLVTConfig Hidden

import Logger

int function init() global
	int conf = JDB.solveObj(".mslVTConfig")
	if !conf
		conf = autoupdate()
	endif
	return conf
endfunction

int function autoupdate() global
	int newConf = JValue.readFromFile("Data/mslVTConfig.json")

	if !newConf
		logDetail("mslVT", "mslVTConfig:autoupdate can't autoupdate, can't parse the config file")
		return JDB.solveObj(".mslVTConfig")
	endif

	int oldV = JDB.solveInt(".mslVTConfig.version", -1)
	int newV = JMap.getInt(newConf, "version", -1)

	if oldV < newV
		JDB.setObj("mslVTConfig", newConf)
		logDetail("mslVT", "MSLVTConfig:autoupdate auto-updating config from v"+oldV+" to v"+newV)
	else
		logDetail("mslVT", "MSLVTConfig:autoupdate already up-to-date")
		JValue.zeroLifetime(newConf)
	endif

	return JDB.solveObj(".mslVTConfig")
endfunction

int function solveObj(string path) global
	return JValue.solveObj(init(), path)
endfunction

int function solveInt(string path, int def = 0) global
	return JValue.solveInt(init(), path, def)
endfunction

float function solveFlt(string path, float def = 0.0) global
	return JValue.solveFlt(init(), path, def)
endfunction

form function solveForm(string path) global
	return JValue.solveForm(init(), path)
endfunction

;;;; Ageing ;;;;;
;/
	"ageing" : {
		"__metaInfo": {"typeName": "JIntMap"},
		"1": {
			"DetNat": 1.0, "BloodMax": 1000, "buff": "__formData|mslVampiricThirst.esm|0x030022F5",
			"AgeReq": 0
		}
	}
/;

int function getAgeing() global
	return solveObj(".ageing")
endfunction
int function getAgeEntry(int age) global
	return solveObj(".ageing["+age+"]")
endfunction
float function solveAgeingFlt(int age, string path) global
	return solveFlt(".ageing["+age+"]" + path)
endfunction
; Function pair below is for MCM
function setDaysReqForAge(float daysRequired, int ageKey) global
	JValue.solveFltSetter(getAgeing(), "["+ageKey+"].AgeReq", daysRequired)
	Debug.Notification("age " + ageKey + ".daysRequired = " + daysRequired)
endfunction

float function getDaysReqForAge(int ageKey) global
	return JValue.solveFlt(getAgeing(), "["+ageKey+"].AgeReq")
endfunction

;;;;;;;;;;;;;;;;;;;;;;


GlobalVariable function getBloodCurr() global
	return JValue.solveForm(init(), ".global.BloodCurr") as GlobalVariable
endfunction

Faction function getNonZeroBloodFaction() global
	return JValue.solveForm(init(), ".global.NonZeroBloodFaction") as Faction
endfunction

float function blood2HpMult() global
	return 10.0
endfunction
