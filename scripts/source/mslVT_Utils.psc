Scriptname mslVT_Utils hidden

function log(string mess) global
	Logger.logDetail("mslVT", mess)
EndFunction

function logEffect(ActiveMagicEffect source, string mess) global
	log(" tototo " + mess)
EndFunction

function logForm(Form source, string mess) global
	log(source + " " + mess)
EndFunction

float function fMax(float a, float b) global
	if a > b
		return a
	endif
	return b
endfunction

float function fMin(float a, float b) global
	if a < b
		return a
	endif
	return b
endfunction
