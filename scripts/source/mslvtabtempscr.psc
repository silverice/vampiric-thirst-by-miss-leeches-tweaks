Scriptname mslVTAbTempSCR extends activemagiceffect    

Event OnEffectStart(Actor akTarget, Actor akCaster)
	mark = GameDaysPassed.GetValue() + duration/24
	RegisterForSingleUpdate(frequency)
Endevent

Event OnUpdate()
	If GameDaysPassed.GetValue() >= mark
		GetTargetActor().RemoveSpell(Current)
	Endif
	RegisterForSingleUpdate(frequency)
Endevent

Event OnCellAttach()
	RegisterForSingleUpdate(frequency)
Endevent

Event OnEffectFinish(Actor akTarget, Actor akCaster)
	GetTargetActor().RemoveSpell(Current)
Endevent

Float Property duration  Auto  
Float Property mark  Auto  
GlobalVariable Property GameDaysPassed  Auto  
Spell Property Current  Auto  
Float Property frequency = 1.0  Auto  
