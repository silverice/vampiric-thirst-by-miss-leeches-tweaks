Scriptname mslVTConstantImodSCR extends activemagiceffect  

event OnEffectStart(Actor akTarget, Actor akCaster)
	if akTarget == Game.GetPlayer()
		Imod.ApplyCrossFade(0.5)
		RegisterForSingleUpdate(1.0)
	endIf
endEvent

event OnUpdate()
	Imod.PopTo(Imod, ImodStrength)
endEvent

event OnEffectFinish(Actor akTarget, Actor akCaster)
	ImodEnd.ApplyCrossFade(0.5)
	Imod.Remove()
endEvent

ImageSpaceModifier Property Imod  Auto  
ImageSpaceModifier Property ImodEnd  Auto 
Float Property ImodStrength Auto