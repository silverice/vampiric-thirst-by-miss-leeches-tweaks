Scriptname mslVTDetNatModSCR extends activemagiceffect  

Event OnEffectStart(Actor akTarget, Actor akCaster)
	DetAct.Mod(Modifier)
Endevent

Event OnEffectFinish(Actor akTarget, Actor akCaster)
	DetAct.Mod(-Modifier)
Endevent

GlobalVariable Property DetAct Auto
Float Property Modifier Auto