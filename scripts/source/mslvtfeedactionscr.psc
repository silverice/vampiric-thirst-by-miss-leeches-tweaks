Scriptname mslVTFeedActionSCR extends activemagiceffect  

Event OnEffectStart(Actor akTarget, Actor akCaster)

	If mslVTMCMQST.CheckNoBite(akCaster)
		;do nothing
	else
		DispelInvisibiliy.Cast(akCaster)

		If akTarget.HasMagicEffectWithKeyword(ParalysisKW)
			;Target is paralysed
			int button = FeedChoice.Show()
			If (button >= 0 && button <= 5)
				DLC1VampireTurn.PlayerBitesMe(akTarget)
				akCaster.StartVampireFeed(akTarget)
				PlayerVampireQuest.VampireFeed(akTarget, button, 0)
			endif
		Elseif akTarget.GetSleepState() == 3
			;Target is sleeping
			int button = FeedChoice.Show()
			If (button >= 0 && button <= 5)
				DLC1VampireTurn.PlayerBitesMe(akTarget)
				akCaster.StartVampireFeed(akTarget)
				PlayerVampireQuest.VampireFeed(akTarget, button, 0)
			Endif
		Elseif (akTarget.HasMagicEffect(DLC1VampireMesmerizeMagicEffect) && (akTarget.HasMagicEffect(mslVTPwSubMesASeductionME) || akTarget.IsInFaction(mslVTPwSubMesBEntrancementFAC))) || (akTarget.IsInFaction(DLC1ThrallFaction))
			;Target is a thrall or under Mesmerism
			int button = FeedChoice.Show()
			If (button >= 0 && button <= 5)
				DLC1VampireTurn.PlayerBitesMe(akTarget)
				akCaster.StartVampireFeed(akTarget)
				PlayerVampireQuest.VampireFeed(akTarget, button, 0)
			endif
		Endif
	Endif

Endevent

PlayerVampireQuestScript Property PlayerVampireQuest  Auto  
dlc1vampireturnscript Property DLC1VampireTurn  Auto  
Sound Property FeedSound Auto
Message Property FeedChoice Auto
Message Property mslVTFeedNoBiteControlsMSG Auto
GlobalVariable Property mslVTSetNoBiteControls Auto
Spell Property DispelInvisibiliy Auto
Keyword Property ParalysisKW Auto
MagicEffect Property DLC1VampireMesmerizeMagicEffect Auto
MagicEffect Property mslVTPwSubMesASeductionME Auto
Faction Property mslVTPwSubMesBEntrancementFAC Auto
Faction Property DLC1ThrallFaction Auto

mslVTMCMDebugSCR Property mslVTMCMQST  Auto  