Scriptname mslVTFeedBottledSCR extends activemagiceffect  

import MSLVTVampire

Event OnEffectStart(Actor akTarget, Actor akCaster)
;	Debug.Notification("There is little taste in this blood but at least it can keep the hunger at bay for a time...")
	float BloodRandom = Utility.RandomInt(1,10)*BloodRatio.GetValue()
	MSLVTVampire.modBlood(akTarget, BloodRandom)
	;BloodCur.Mod(BloodRandom)
	akTarget.RestoreAV("Health", BloodRandom/2)
	akTarget.AddItem(EmptyBottles.GetAt(Utility.RandomInt(1, EmptyBottles.GetSize())), 1)
Endevent

;GlobalVariable Property BloodCur Auto
GlobalVariable Property BloodRatio Auto
FormList Property EmptyBottles Auto