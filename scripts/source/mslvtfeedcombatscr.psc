Scriptname mslVTFeedCombatSCR extends activemagiceffect  

Event OnEffectStart(Actor akTarget, Actor akCaster)

	If mslVTMCMQST.CheckNoBite(akCaster)
		;do nothing
	else
		DispelInvisibiliy.Cast(akCaster)

		float HealthDiff = (akCaster.GetAV("Health")-akTarget.GetAV("Health"))/2
		float StaminaDiff = (akCaster.GetAV("Stamina")-akTarget.GetAV("Stamina"))/2
		float UnarmedDiff = (akCaster.GetAV("UnarmedDamage")-akTarget.GetAV("UnarmedDamage"))
		float BlockMod = -akTarget.GetAV("Block")
		float HungerMod = PlayerVampireQuest.VampireStatus*10
		float LOSMod = 0
		if !akTarget.HasLOS(akCaster)
			LOSMod = 25
		endif
		float DSMod = 0
		if akCaster.HasSpell(DeadlyStrengthSP)
			DSMod = 5*mslVTAge.GetValue()
		endif
		float SuccessChance = HealthDiff+StaminaDiff+UnarmedDiff+BlockMod+HungerMod+LOSMod+DSMod
		if akTarget.IsBleedingOut()
			SuccessChance = 100
		elseif akTarget.HasMagicEffectWithKeyword(CalmKW)
			SuccessChance += Math.Abs(SuccessChance*0.5)
		endif

		;Debug.MessageBox("Success chance is: " + SuccessChance + "%")

		If mslVTSetKillEssential.GetValue() == 0 && akTarget.IsEssential()
			FailStagger.Cast(akTarget, akCaster)
			FailAssault.Cast(akCaster, akTarget)
			;akTarget.SendAssaultAlarm()
			;Debug.Notification("My attempt to bite was resisted!")
			FeedFail1MSG.Show()
			akTarget.SetFactionRank(mslVTMasqPlayerKnownFac, -1)
		Else
			If akCaster.IsSneaking() == 1 && akCaster.IsDetectedBy(akTarget) == 0
				If SuccessChance+akCaster.GetAV("Sneak")/2 > 0
					if !akCaster.IsWeaponDrawn()
						int button = FeedChoice.Show()
						If (button >= 0 && button <= 5)
							DLC1VampireTurn.PlayerBitesMe(akTarget)
							akCaster.PlayIdleWithTarget(FeedCombatIdle2, akTarget)
							PlayerVampireQuest.VampireFeed(akTarget, button, 1)
						endif
					else
						FeedSound.Play(akCaster)
						;Debug.Notification("Sneak feeding started!")
						If akCaster.GetEquippedItemType(0) > 0 || akCaster.GetEquippedItemType(1) > 0
							akCaster.EquipItem(UnarmedEquip, false, true)
							utility.wait(0.5)
						Endif
						akTarget.PlayIdleWithTarget(FeedSneakIdle1, akCaster)
						PlayerVampireQuest.VampireFeed(akTarget, 4, 0)
					endif
				Else
					FailStagger.Cast(akTarget, akCaster)
					FailAssault.Cast(akCaster, akTarget)
					;Debug.Notification("The prey slipped from my grasp!")
					FeedFail2MSG.Show()
					akTarget.SetFactionRank(mslVTMasqPlayerKnownFac, -1)
				Endif
			Else
				If SuccessChance > 0
					if !akCaster.IsWeaponDrawn()
						int button = FeedChoice.Show()
						If (button >= 0 && button <= 5)
							DLC1VampireTurn.PlayerBitesMe(akTarget)
							akCaster.PlayIdleWithTarget(FeedCombatIdle1, akTarget)
							PlayerVampireQuest.VampireFeed(akTarget, button, 1)
							akTarget.SetFactionRank(mslVTMasqPlayerKnownFac, -1)
						endif
					else
						akCaster.PlayIdleWithTarget(FeedCombatIdle1, akTarget)
						PlayerVampireQuest.VampireFeed(akTarget, 4, 0)
						;Debug.Notification("Combat feeding started!")
					endif
				Else
					FailStagger.Cast(akTarget, akCaster)
					FailAssault.Cast(akCaster, akTarget)
					;akTarget.SendAssaultAlarm()
					;Debug.Notification("My attempt at grappling was deflected!")
					FeedFail3MSG.Show()
					akTarget.SetFactionRank(mslVTMasqPlayerKnownFac, -1)
				Endif
			Endif
		Endif
	Endif

Endevent

Idle Property FeedCombatIdle1 Auto
Idle Property FeedCombatIdle2 Auto
Idle Property FeedSneakIdle1 Auto
Idle Property FeedNeutralIdle1 Auto
Idle Property FeedFailIdle1 Auto
PlayerVampireQuestScript Property PlayerVampireQuest  Auto  
dlc1vampireturnscript Property DLC1VampireTurn  Auto  
Weapon Property UnarmedEquip Auto
Sound Property FeedSound Auto
Keyword Property CalmKW Auto
Message Property FeedChoice Auto
Message Property FeedFail1MSG Auto
Message Property FeedFail2MSG Auto
Message Property FeedFail3MSG Auto
Message Property mslVTFeedNoBiteControlsMSG Auto
GlobalVariable Property mslVTSetNoBiteControls Auto
Spell Property FailStagger Auto
Spell Property FailAssault Auto
Spell Property DispelInvisibiliy Auto
GlobalVariable Property mslVTSetKillEssential Auto
GlobalVariable Property mslVTAge Auto
Spell Property DeadlyStrengthSP Auto
Faction Property mslVTMasqPlayerKnownFac Auto

mslVTMCMDebugSCR Property mslVTMCMQST  Auto  