Scriptname mslVTFeedDeadSCR extends activemagiceffect  

Event OnEffectStart(Actor akTarget, Actor akCaster)
	If mslVTMCMQST.CheckNoBite(akCaster)
		;do nothing
	else
		akCaster.StartCannibal(akTarget)
		PlayerVampireQuest.VampireFeed(akTarget, -1, 0)
	Endif
Endevent

PlayerVampireQuestScript Property PlayerVampireQuest  Auto 
Message Property mslVTFeedNoBiteControlsMSG Auto
GlobalVariable Property mslVTSetNoBiteControls Auto

mslVTMCMDebugSCR Property mslVTMCMQST  Auto  