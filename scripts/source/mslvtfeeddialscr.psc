Scriptname mslVTFeedDialSCR extends Quest  

Function SeductionCheck(Actor akSeducer, Actor akSeduced)

	;Calculating the seducer's base score

	float srSpeechBaseMod = akSeducer.GetAV("Speechcraft")
	float srAllurePerkMod = 0
	if akSeducer.HasPerk(Allure)
		srAllurePerkMod = 15
	endif
	float srPersuasionPerkMod = 0
	if akSeducer.HasPerk(Persuasion)
		srPersuasionPerkMod = 10
	endif
	float srIntimidationPerkMod = 0
	if akSeducer.HasPerk(Intimidation)
		srIntimidationPerkMod = 15
	endif
	float srHypnoticGazePerkMod = 0
	if akSeducer.HasPerk(HypnoticGaze)
		srHypnoticGazePerkMod = 10
	endif
	float srAspectOfTerrorPerkMod = 0
	if akSeducer.HasPerk(AspectOfTerror)
		srAspectOfTerrorPerkMod = 15
	endif
	float srClothingMod = 0
	float srClothingIntMod = 0
	if akSeducer.WornHasKeyword(ClothingRich)
		srClothingMod = 20
		srClothingIntMod = 10
	elseif akSeducer.WornHasKeyword(ClothingPoor)
		srClothingMod = -20
		srClothingIntMod = -10
	endif
	float srArmourMod = 0
	if akSeducer.WornHasKeyword(ArmorMaterialDaedric) || akSeducer.WornHasKeyword(ArmorMaterialDragonplate)
		srArmourMod = 30
	elseif akSeducer.WornHasKeyword(ArmorMaterialOrcish) || akSeducer.WornHasKeyword(ArmorDarkBrotherhood) || akSeducer.WornHasKeyword(ArmorNightingale) || akSeducer.WornHasKeyword(ArmorMaterialDragonscale) || akSeducer.WornHasKeyword(ArmorMaterialEbony) || akSeducer.WornHasKeyword(ArmorMaterialThievesGuildLeader)
		srArmourMod = 25
	elseif akSeducer.WornHasKeyword(ArmorMaterialFalmer) || akSeducer.WornHasKeyword(ArmorMaterialBlades) || akSeducer.WornHasKeyword(ArmorMaterialDwarven) || akSeducer.WornHasKeyword(ArmorMaterialBearStormcloak) || akSeducer.WornHasKeyword(ArmorMaterialPenitus) || akSeducer.WornHasKeyword(ArmorMaterialSteelPlate) || akSeducer.WornHasKeyword(ArmorMaterialThievesGuild) || akSeducer.WornHasKeyword(ArmorMaterialGlass)
		srArmourMod = 20
	elseif akSeducer.WornHasKeyword(ArmorMaterialImperialHeavy) || akSeducer.WornHasKeyword(ArmorMaterialSteel) || akSeducer.WornHasKeyword(ArmorMaterialScaled) || akSeducer.WornHasKeyword(ArmorMaterialElven) || akSeducer.WornHasKeyword(ArmorMaterialElvenGilded)
		srArmourMod = 15
	elseif akSeducer.WornHasKeyword(ArmorMaterialIronBanded) || akSeducer.WornHasKeyword(ArmorMaterialStormcloak) || akSeducer.WornHasKeyword(ArmorMaterialMS02Forsworn) || akSeducer.WornHasKeyword(ArmorMaterialForsworn) || akSeducer.WornHasKeyword(ArmorMaterialImperialLight) || akSeducer.WornHasKeyword(ArmorMaterialImperialStudded)
		srArmourMod = 10
	elseif akSeducer.WornHasKeyword(ArmorMaterialIron) || akSeducer.WornHasKeyword(ArmorMaterialStudded) || akSeducer.WornHasKeyword(ArmorMaterialLeather) || akSeducer.WornHasKeyword(ArmorMaterialHide)
		srArmourMod = 5
	endif
	float srRaceMod = 0
	float srRaceIntMod = 0
	if DunmerRaceFL.HasForm(akSeducer.GetLeveledActorBase().GetRace()) && akSeducer.GetLeveledActorBase().GetSex() == 1
		srRaceMod = 0
	elseif OrsimerRaceFL.HasForm(akSeducer.GetLeveledActorBase().GetRace()) && !OrsimerRaceFL.HasForm(akSeduced.GetLeveledActorBase().GetRace())
		srRaceMod = -20
		srRaceIntMod = 15
	elseif KhajiitRaceFL.HasForm(akSeducer.GetLeveledActorBase().GetRace()) && !KhajiitRaceFL.HasForm(akSeduced.GetLeveledActorBase().GetRace())
		srRaceIntMod = 10
	elseif akSeducer.GetLeveledActorBase().GetRace().GetName() != akSeduced.GetLeveledActorBase().GetRace().GetName()
		srRaceMod = -10
	endif
	;Debug.Notification("Seducer's race: " + akSeducer.GetLeveledActorBase().GetRace().GetName() + " Seduced's race: " + akSeduced.GetLeveledActorBase().GetRace().GetName())
	float srRelationshipMod = akSeduced.GetRelationshipRank(akSeducer)*5
	float srRelationshipRevMod = akSeduced.GetRelationshipRank(akSeducer)*10
	float srSexMod = 0
	float srSexIntMod = 0
	if akSeducer.GetLeveledActorBase().GetSex() == 1
		srSexMod = 15
	elseif akSeducer.GetLeveledActorBase().GetSex() == 0
		srSexIntMod = 15
	endif
	float srBloodPrefMult = 1
	if akSeduced.GetLeveledActorBase().GetSex() == 0
		if akSeducer.HasSpell(mslVTBloodSexMaleME)
			srBloodPrefMult = 1.25
		endif
	elseif akSeduced.GetLeveledActorBase().GetSex() == 1
		if akSeducer.HasSpell(mslVTBloodSexFemaleME)
			srBloodPrefMult = 1.25
		endif
	endif
	float srSameFactionMod
	if akSeducer.GetFactionReaction(akSeduced) == 2
		srSameFactionMod = 25
	endif

	float srBaseScoreSed = (srSpeechBaseMod+srAllurePerkMod+srPersuasionPerkMod+srHypnoticGazePerkMod+srClothingMod+srRaceMod+srRelationshipMod+srSexMod)*srBloodPrefMult
	float srBaseScoreInt = (srSpeechBaseMod+srIntimidationPerkMod+srAspectOfTerrorPerkMod+srClothingMod+srArmourMod+srRaceIntMod+srRelationshipMod+srSexIntMod+(akSeducer.GetAV("Health")/2))*srBloodPrefMult
	float srBaseScoreRev = (srSpeechBaseMod+srAllurePerkMod+srPersuasionPerkMod+srHypnoticGazePerkMod+srRelationshipRevMod+srSameFactionMod)*srBloodPrefMult
	;Debug.Notification("Seducer's base seduction score is: " + srBaseScoreSed)
	;Debug.Notification("Seducer's base intimidation score is: " + srBaseScoreInt)
	;Debug.Notification("Seducer's base reveal score is: " + srBaseScoreRev)

	;Calculating the seduced's base score

	float sdSpeechBaseMod = akSeduced.GetAV("Speechcraft")
	float sdAllurePerkMod = 0
	if akSeduced.HasPerk(Allure)
		sdAllurePerkMod = 15
	endif
	float sdPersuasionPerkMod = 0
	if akSeduced.HasPerk(Persuasion)
		sdPersuasionPerkMod = 10
	endif
	float sdIntimidationPerkMod = 0
	if akSeduced.HasPerk(Intimidation)
		sdIntimidationPerkMod = 15
	endif
	float sdHypnoticGazePerkMod = 0
	if akSeduced.HasPerk(HypnoticGaze)
		sdHypnoticGazePerkMod = 10
	endif
	float sdAspectOfTerrorPerkMod = 0
	if akSeduced.HasPerk(AspectOfTerror)
		sdAspectOfTerrorPerkMod = 15
	endif
	float sdMasterOfTheMindPerkMod = 0
	if akSeduced.HasPerk(MasterOfTheMind)
		sdMasterOfTheMindPerkMod = 30
	endif
	float sdRagePerkMod = 0
	if akSeduced.HasPerk(Rage)
		sdRagePerkMod = 20
	endif
	float sdClothingMod = 0
	float sdClothingIntMod = 0
	if akSeduced.WornHasKeyword(ClothingRich)
		sdClothingMod = 20
		sdClothingIntMod = 10
	elseif akSeduced.WornHasKeyword(ClothingPoor)
		sdClothingMod = -20
		sdClothingIntMod = -10
	endif
	float sdArmourMod = 0
	if akSeduced.WornHasKeyword(ArmorMaterialDaedric) || akSeduced.WornHasKeyword(ArmorMaterialDragonplate)
		sdArmourMod = 30
	elseif akSeduced.WornHasKeyword(ArmorMaterialOrcish) || akSeduced.WornHasKeyword(ArmorDarkBrotherhood) || akSeduced.WornHasKeyword(ArmorNightingale) || akSeduced.WornHasKeyword(ArmorMaterialDragonscale) || akSeduced.WornHasKeyword(ArmorMaterialEbony) || akSeduced.WornHasKeyword(ArmorMaterialThievesGuildLeader)
		sdArmourMod = 25
	elseif akSeduced.WornHasKeyword(ArmorMaterialFalmer) || akSeduced.WornHasKeyword(ArmorMaterialBlades) || akSeduced.WornHasKeyword(ArmorMaterialDwarven) || akSeduced.WornHasKeyword(ArmorMaterialBearStormcloak) || akSeduced.WornHasKeyword(ArmorMaterialPenitus) || akSeduced.WornHasKeyword(ArmorMaterialSteelPlate) || akSeduced.WornHasKeyword(ArmorMaterialThievesGuild) || akSeduced.WornHasKeyword(ArmorMaterialGlass)
		sdArmourMod = 20
	elseif akSeduced.WornHasKeyword(ArmorMaterialImperialHeavy) || akSeduced.WornHasKeyword(ArmorMaterialSteel) || akSeduced.WornHasKeyword(ArmorMaterialScaled) || akSeduced.WornHasKeyword(ArmorMaterialElven) || akSeduced.WornHasKeyword(ArmorMaterialElvenGilded)
		sdArmourMod = 15
	elseif akSeduced.WornHasKeyword(ArmorMaterialIronBanded) || akSeduced.WornHasKeyword(ArmorMaterialStormcloak) || akSeduced.WornHasKeyword(ArmorMaterialMS02Forsworn) || akSeduced.WornHasKeyword(ArmorMaterialForsworn) || akSeduced.WornHasKeyword(ArmorMaterialImperialLight) || akSeduced.WornHasKeyword(ArmorMaterialImperialStudded)
		sdArmourMod = 10
	elseif akSeduced.WornHasKeyword(ArmorMaterialIron) || akSeduced.WornHasKeyword(ArmorMaterialStudded) || akSeduced.WornHasKeyword(ArmorMaterialLeather) || akSeduced.WornHasKeyword(ArmorMaterialHide)
		sdArmourMod = 5
	endif
	float sdRaceMod = 0
	float sdRaceIntMod = 0
	if DunmerRaceFL.HasForm(akSeduced.GetLeveledActorBase().GetRace()) && akSeduced.GetLeveledActorBase().GetSex() == 1
		sdRaceMod = -10
	elseif AltmerRaceFL.HasForm(akSeduced.GetLeveledActorBase().GetRace())
		sdRaceMod = 15
	elseif OrsimerRaceFL.HasForm(akSeduced.GetLeveledActorBase().GetRace()) && !OrsimerRaceFL.HasForm(akSeducer.GetLeveledActorBase().GetRace())
		sdRaceIntMod = 15
	elseif KhajiitRaceFL.HasForm(akSeduced.GetLeveledActorBase().GetRace()) && !KhajiitRaceFL.HasForm(akSeducer.GetLeveledActorBase().GetRace())
		sdRaceIntMod = 10
	endif
	float sdRelationshipMod = 0
	if akSeduced.GetRelationshipRank(akSeducer) < akSeduced.GetHighestRelationshipRank()
		sdRelationshipMod = 20
	endif
	float sdSexMod = 0
	float sdSexIntMod = 0
	if akSeduced.GetLeveledActorBase().GetSex() == 1
		sdSexMod = 15
	elseif akSeduced.GetLeveledActorBase().GetSex() == 0
		sdSexIntMod = 15
	endif
	float sdFactionMod = 0
	float sdFactionIntMod = 0
	float sdFactionRevMod = 0
	int index = 0
	while (Index < mslVTFDsdResFactionsFL.GetSize())
		if akSeduced.IsInFaction(mslVTFDsdResFactionsFL.GetAt(Index) as Faction)
			sdFactionMod = 30
			sdFactionIntMod = 40
			sdFactionRevMod = 50
			Index = mslVTFDsdResFactionsFL.GetSize()
		else
			Index += 1
		endif
	endwhile
	float sdMesmerismMod = 0
	if akSeduced.HasMagicEffect(MesSeductionME)
		sdMesmerismMod = -15
		if akSeduced.IsInFaction(MesEntrancementFAC)
			sdMesmerismMod = -40
		endif
	endif
	float sdVampireMod = 0
	if akSeduced.HasKeyword(Vampire)
		sdVampireMod = 30
	endif
	float sdVampirismKnownMod = 0
	float sdVampirismKnownIntMod = 0
	float sdVampirismKnownRevMod = 0
	if akSeduced.GetFactionRank(mslVTMasqPlayerKnownFac) == -1
		sdVampirismKnownMod = 15
		sdVampirismKnownIntMod = -20
		sdVampirismKnownRevMod = 30
	elseif akSeduced.GetFactionRank(mslVTMasqPlayerKnownFac) == 1
		sdVampirismKnownMod = -15
		sdVampirismKnownIntMod = -10
		sdVampirismKnownRevMod = -30
	endif
	float sdMoralitySedMod = akSeduced.GetAV("Morality")*5
	float sdMoralityRevMod = akSeduced.GetAV("Morality")*10
	float sdMoralityIntMod = akSeduced.GetAV("Morality")*5
	float sdAggressionSedMod = akSeduced.GetAV("Aggression")*10
	float sdAggressionRevMod = akSeduced.GetAV("Aggression")*5
	float sdAggressionIntMod = akSeduced.GetAV("Aggression")*5
	float sdConfidenceSedMod = akSeduced.GetAV("Confidence")*5
	float sdConfidenceRevMod = akSeduced.GetAV("Confidence")*5
	float sdConfidenceIntMod = akSeduced.GetAV("Confidence")*10
	float sdAISedMod = sdMoralitySedMod+sdAggressionSedMod+sdConfidenceSedMod
	float sdAIRevMod = sdMoralityRevMod+sdAggressionRevMod+sdConfidenceRevMod
	float sdAIIntMod = sdMoralityIntMod+sdAggressionIntMod+sdConfidenceIntMod
	float sdIntimidatedSedMod = 0
	float sdIntimidatedRevMod = 0
	float sdIntimidatedIntMod = 0
	if akSeduced.IsInFaction(mslVTFeedDialogueLastingIntFAC)
		sdIntimidatedSedMod = 20
		sdIntimidatedRevMod = 30
		sdIntimidatedIntMod = 10
	endif


	float sdBaseScoreSed = sdSpeechBaseMod*2+sdAllurePerkMod+sdPersuasionPerkMod+sdMasterOfTheMindPerkMod+sdClothingMod+sdRaceMod+\
		sdRelationshipMod+sdSexMod+sdFactionMod+sdMesmerismMod+sdVampireMod+sdVampirismKnownMod+sdAISedMod+sdIntimidatedSedMod

	float sdBaseScoreInt = sdSpeechBaseMod*2+sdIntimidationPerkMod+sdAspectOfTerrorPerkMod+sdRagePerkMod+sdClothingMod+sdArmourMod+\
		sdRaceIntMod+sdSexIntMod+sdFactionIntMod+sdMesmerismMod+sdVampireMod+sdVampirismKnownIntMod+(akSeduced.GetAV("Health")/2)+sdAIIntMod+sdIntimidatedIntMod 
		
	float sdBaseScoreRev = sdSpeechBaseMod*2+sdAllurePerkMod+sdPersuasionPerkMod+sdMasterOfTheMindPerkMod+sdFactionRevMod+\
		sdMesmerismMod+sdVampireMod+sdVampirismKnownRevMod+sdAIRevMod+sdIntimidatedRevMod
	;Debug.Notification("Seducee's base seduction score is: " + sdBaseScoreSed)
	;Debug.Notification("Seducee's base intimidation score is: " + sdBaseScoreInt)
	;Debug.Notification("Seducee's base reveal score is: " + sdBaseScoreRev)

	;Calculating final scores

	mslVTFDsrToneSubtle.SetValue(srBaseScoreSed+Utility.RandomInt(-15,10))
	mslVTFDsdToneSubtle.SetValue(sdBaseScoreSed+mslVTFDDifficulty.Value+Utility.RandomInt(-10,15))
	mslVTFDsrToneAggressive.SetValue(srBaseScoreInt+Utility.RandomInt(-15,15))
	mslVTFDsdToneAggressive.SetValue(sdBaseScoreInt+mslVTFDDifficulty.Value+Utility.RandomInt(-15,15))
	mslVTFDsrToneCharming.SetValue(srBaseScoreRev+Utility.RandomInt(-10,5))
	mslVTFDsdToneCharming.SetValue(sdBaseScoreRev+mslVTFDDifficulty.Value+Utility.RandomInt(-5,10))

	;Debug.Notification("Function finished!")

Endfunction

Perk Property Allure Auto
Perk Property Persuasion Auto
Perk Property HypnoticGaze Auto
Perk Property Intimidation Auto
Perk Property MasterOfTheMind Auto
Perk Property AspectOfTerror Auto
Perk Property Rage Auto
Keyword Property ClothingRich Auto
Keyword Property ClothingPoor Auto
Keyword Property ArmorMaterialDaedric Auto
Keyword Property ArmorMaterialDragonplate Auto
Keyword Property ArmorMaterialOrcish Auto
Keyword Property ArmorDarkBrotherhood Auto
Keyword Property ArmorNightingale Auto
Keyword Property ArmorMaterialDragonscale Auto
Keyword Property ArmorMaterialEbony Auto
Keyword Property ArmorMaterialThievesGuildLeader Auto
Keyword Property ArmorMaterialFalmer Auto
Keyword Property ArmorMaterialBlades Auto
Keyword Property ArmorMaterialDwarven Auto
Keyword Property ArmorMaterialBearStormcloak Auto
Keyword Property ArmorMaterialPenitus Auto
Keyword Property ArmorMaterialSteelPlate Auto
Keyword Property ArmorMaterialThievesGuild Auto
Keyword Property ArmorMaterialGlass Auto
Keyword Property ArmorMaterialImperialHeavy Auto
Keyword Property ArmorMaterialSteel Auto
Keyword Property ArmorMaterialScaled Auto
Keyword Property ArmorMaterialElven Auto
Keyword Property ArmorMaterialElvenGilded Auto
Keyword Property ArmorMaterialIronBanded Auto
Keyword Property ArmorMaterialStormcloak Auto
Keyword Property ArmorMaterialMS02Forsworn Auto
Keyword Property ArmorMaterialForsworn Auto
Keyword Property ArmorMaterialImperialLight Auto
Keyword Property ArmorMaterialImperialStudded Auto
Keyword Property ArmorMaterialIron Auto
Keyword Property ArmorMaterialStudded Auto
Keyword Property ArmorMaterialLeather Auto
Keyword Property ArmorMaterialHide Auto
Keyword Property Vampire Auto
FormList Property DunmerRaceFL Auto
FormList Property AltmerRaceFL Auto
FormList Property OrsimerRaceFL Auto
FormList Property KhajiitRaceFL Auto
FormList Property mslVTFDsdResFactionsFL Auto
Faction Property mslVTMasqPlayerKnownFac Auto
Faction Property mslVTFeedDialogueLastingIntFAC Auto
MagicEffect Property SunPenaltyME Auto
MagicEffect Property SunDamageME Auto
MagicEffect Property MesSeductionME Auto
Faction Property MesEntrancementFAC Auto
MagicEffect Property mslVTBloodSexFemaleME Auto
MagicEffect Property mslVTBloodSexMaleME Auto

GlobalVariable Property mslVTFDsrToneSubtle Auto
GlobalVariable Property mslVTFDsdToneSubtle Auto
GlobalVariable Property mslVTFDsrToneAggressive Auto
GlobalVariable Property mslVTFDsdToneAggressive Auto
GlobalVariable Property mslVTFDsrToneCharming Auto
GlobalVariable Property mslVTFDsdToneCharming Auto

GlobalVariable Property mslVTFDDifficulty Auto