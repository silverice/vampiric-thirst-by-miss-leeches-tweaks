Scriptname mslVTFeedTokenControl extends activemagiceffect  

float Property TimeOld Auto
float Property TimeNew Auto
float Property TimePassed Auto

Event OnEffectStart(Actor akTarget, Actor akCaster)

	;Debug.MessageBox("Damage manager: started!")

	TimeNew = GameDaysPassed.GetValue()
	TimePassed = TimeNew - GetTargetActor().GetAV("Fame")
	If GetTargetActor().GetAV("Fame") != 0
		GetTargetActor().ModAV("Variable08", -(0.5*TimePassed))
	Endif

	if GetTargetActor().GetAV("Health") > (GetTargetActor().GetBaseAV("Health")-(GetTargetActor().GetBaseAV("Health")*GetTargetActor().GetAV("Variable08")))
		GetTargetActor().RestoreAV("Health", GetTargetActor().GetBaseAV("Health"))
		GetTargetActor().DamageAV("Health", (GetTargetActor().GetBaseAV("Health")*(GetTargetActor().GetAV("Variable08"))))
	endif

	GetTargetActor().ForceAV("Fame", GameDaysPassed.GetValue())

	if GetTargetActor().GetAV("Variable08") <= 0
		;Debug.Notification("Damage manager: work done! SELF DESTRUCTING NOW!")
		GetTargetActor().ModAV("Variable08", math.Abs(GetTargetActor().GetAV("Variable08")))
		GetTargetActor().RestoreAV("Health", GetTargetActor().GetBaseAV("Health"))
		GetTargetActor().RemoveFromFaction(mslVTFeedRecoveryFAC)
	endif

	;Debug.Notification("Damage manager: update finished!")

Endevent

GlobalVariable Property GameDaysPassed Auto
Faction Property mslVTFeedRecoveryFAC Auto