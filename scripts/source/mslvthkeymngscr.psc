Scriptname mslVTHkeyMngSCR extends activemagiceffect  

import MSLVTVampire

Event OnEffectStart(Actor akTarget, Actor akCaster)
	If akTarget == Game.GetPlayer()
		RegisterForSingleUpdate(0.1)
	Endif
Endevent

Event OnUpdate()
	Actor akTarget = GetTargetActor()
	If Input.IsKeyPressed(mslVTKeyFeed.GetValueInt())
		;Debug.MessageBox("Pressed the feed key!")
		FeedActionSP.Cast(akTarget)
	Elseif Input.IsKeyPressed(mslVTKeyStatus.GetValueInt())
		;Debug.MessageBox("Pressed the status key!")
		StatusCheckMSG.Show(MSLVTVampire.getBlood(akTarget)/MSLVTVampire.getBloodMax(akTarget)*100)
	Endif
	RegisterForSingleUpdate(0.1)
Endevent

GlobalVariable Property mslVTKeyFeed Auto
GlobalVariable Property mslVTKeyStatus Auto

Spell Property FeedActionSP Auto
Message Property StatusCheckMSG Auto
