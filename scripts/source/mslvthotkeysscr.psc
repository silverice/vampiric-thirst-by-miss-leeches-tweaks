Scriptname mslVTHotkeysSCR extends Quest  

import MSLVTVampire

Event OnInit()
	RegisterHotkeys()
EndEvent

Function RegisterHotkeys()
	RegisterForKey(mslVTKeyFeed.GetValueInt())
	RegisterForKey(mslVTKeyStatus.GetValueInt())
	RegisterForKey(mslVTKeyPSMenu.GetValueInt())
EndFunction

Function UnRegisterHotkeys()
	UnRegisterForKey(mslVTKeyFeed.GetValueInt())
	UnRegisterForKey(mslVTKeyStatus.GetValueInt())
	UnRegisterForKey(mslVTKeyPSMenu.GetValueInt())
EndFunction

Event OnKeyDown(int keyCode)
	if Utility.IsInMenuMode() || PlayerIsVampire.GetValue() <= 0
		;Debug.MessageBox("I don't like when you press my hotkeys this way!")
		return
	endif

	Actor caster = Game.GetPlayer()

	If keyCode == mslVTKeyFeed.GetValueInt()
		;Debug.MessageBox("Pressed the feed key!")
		FeedActionSP.Cast(caster)
	Elseif keyCode == mslVTKeyStatus.GetValueInt()
		;Debug.MessageBox("Pressed the status key!")
		StatusCheckMSG.Show(MSLVTVampire.getBlood(caster)/MSLVTVampire.getBloodMax(caster)*100)
	Elseif keyCode == mslVTKeyPSMenu.GetValueInt()
		;Debug.MessageBox("Pressed the power selection menu key!")
		PSMenuSP.Cast(caster)
	Endif
EndEvent

GlobalVariable Property PlayerIsVampire Auto

GlobalVariable Property mslVTKeyFeed Auto
GlobalVariable Property mslVTKeyStatus Auto
GlobalVariable Property mslVTKeyPSMenu Auto

Spell Property FeedActionSP Auto
Spell Property PSMenuSP Auto
Message Property StatusCheckMSG Auto
