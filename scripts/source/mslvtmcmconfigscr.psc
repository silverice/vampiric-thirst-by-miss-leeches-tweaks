Scriptname mslVTMCMConfigSCR extends SKI_ConfigBase  

; SCRIPT VERSION
int function GetVersion()
	return 2
endFunction

event OnConfigInit()
	; MSLVTConfig.autoupdate()

	Pages = new string[4]
	Pages[0] = "$mslVTbasic"	;Basic
	Pages[1] = "$mslVTadvanced"	;Advanced
	Pages[2] = "$mslVThelptutorials"	;Help & Tutorials
	Pages[3] = "$mslVTcompatibility"	;Compatibility
endEvent

event OnVersionUpdate(int a_version)
	if (a_version > 1)
		OnConfigInit()
	endIf
endEvent

;Variables
GlobalVariable Property mslVTSetTurnToAsh Auto
GlobalVariable Property mslVTSetDialSpeech Auto
GlobalVariable Property mslVTSetDeathSceneAnim Auto
GlobalVariable Property mslVTSetNoBiteControls Auto
GlobalVariable Property mslVTFeedDelay Auto
GlobalVariable Property mslVTFDDifficulty Auto

GlobalVariable Property mslVTSunDmg Auto
GlobalVariable Property mslVTSunAlert Auto
GlobalVariable Property mslVTSunDawn Auto
GlobalVariable Property mslVTSunDusk Auto

GlobalVariable Property mslVTBloodRatio Auto
GlobalVariable Property mslVTBloodAddictionThreshold Auto
GlobalVariable Property mslVTSetBloodQuality Auto
GlobalVariable Property mslVTBloodPercentMult Auto

GlobalVariable Property mslVTSetKillEssential Auto
GlobalVariable Property mslVTSetCloakOff Auto

; GlobalVariable Property mslVTAgeReq1 Auto
; GlobalVariable Property mslVTAgeReq2 Auto
; GlobalVariable Property mslVTAgeReq3 Auto
; GlobalVariable Property mslVTAgeReq4 Auto
GlobalVariable Property mslVTSetAgeingMult Auto
GlobalVariable Property mslVTAge Auto

GlobalVariable Property mslVTAdvVLReqBase Auto
GlobalVariable Property mslVTAdvVLReqIncrement Auto
GlobalVariable Property mslVTExpMult Auto

GlobalVariable Property mslVTKeyFeed Auto
GlobalVariable Property mslVTKeyStatus Auto
GlobalVariable Property mslVTKeyPSMenu Auto

GlobalVariable Property mslVTSetAppearance Auto

GlobalVariable Property mslVTComRNDBottles Auto

GlobalVariable Property mslVTSetHealMode Auto
GlobalVariable Property mslVTSetRegenMode Auto

int reinitOID_S
int btn_dumpPlr
int btn_readPlr

;OIDs
int SunDamageOID_S
int SunAlertOID_S
int SunDawnOID_S
int SunDuskOID_S
int TurnToAshOID_B
int DialSpeechOID_B
int DeathSceneOID_B
int NoBiteControlsOID_B
int DialDifficultyOID_S
int FeedDelayOID_S
int BloodRatioOID_S
int BloodAddictionThresholdOID_S
int BloodQualityOID_T
int BloodPercentMultOID_S
int DebConvertOID_T
int DebUpdateOID_T
int DebUninstallOID_T
int AppearanceOID_T

int KillEssentialOID_B
int CloakOffOID_B
int AgeReq1OID_S
int AgeReq2OID_S
int AgeReq3OID_S
int AgeReq4OID_S
int AgeingMultOID_S
int AgeOID_T
int VLReqBaseOID_S
int VLReqIncrementOID_S
int ExpMultOID_S
int KeyFeedOID_K
int KeyStatusOID_K
int KeyPSMenuOID_K
int HealModeOID_T
int RegenModeOID_T

int Info1OID_T
int Info2OID_T
int Info3OID_T
int Info4OID_T
int Info5OID_T
int faq1OID_T
int faq2OID_T
int faq3OID_T
int faq4OID_T
int faq5OID_T
int faq6OID_T
int faq7OID_T
int faq8OID_T
int tut1OID_T
int tut2OID_T
int tut3OID_T
int tut4OID_T
int tut5OID_T
int tut6OID_T
int tut7OID_T
int tut8OID_T
int tut9OID_T
int tut10OID_T
int tut11OID_T
int tut12OID_T
int tut13OID_T
int tut14OID_T
int tut15OID_T
int tut16OID_T
int tut17OID_T
int tut18OID_T

int comRNDBottlesOID_B

event OnInit()
	;MSLVTConfig.autoupdate()
EndEvent

event OnPageReset(string page)
	if (page == "")
		LoadCustomContent("mslVampiricThirst/mslVTTitle.dds", 75, 52)
		return
	else
		UnloadCustomContent()
	endIf

	if (page == "$mslVTbasic")						;Basic
		;Most often used and safe options
		SetCursorFillMode(TOP_TO_BOTTOM)
		SetCursorPosition(0)

		AddHeaderOption("$mslVTgeneral")			;General
		TurnToAshOID_B = AddToggleOption("$mslVTturntoashO", mslVTSetTurnToAsh.Value)	;Turn To Ash
		DialSpeechOID_B = AddToggleOption("$mslVTdialspeechO", mslVTSetDialSpeech.Value)	;Speech Gains
		DeathSceneOID_B = AddToggleOption("$mslVTdeathsceneO", mslVTSetDeathSceneAnim.Value)	;Animated Transformation
		NoBiteControlsOID_B = AddToggleOption("$mslVTnobitecontrolsO", mslVTSetNoBiteControls.Value)	;Feeding Restrictions
		DialDifficultyOID_S = AddSliderOption("$mslVTdialdifficultyO", mslVTFDDifficulty.Value, "{0}")	;Dialogue Difficulty
		FeedDelayOID_S = AddSliderOption("$mslVTfeeddelayO", mslVTFeedDelay.Value, "{2}")	;Feed Animation Sync

		reinitOID_S = AddToggleOption("Re-Init", true)
		btn_dumpPlr = AddToggleOption("Dump Player", true)
		btn_readPlr = AddToggleOption("Read Player", true)

		AddEmptyOption()

		AddHeaderOption("$mslVTsundamage")	;Sun Damage
		SunDamageOID_S = AddSliderOption("$mslVTsundamageO", mslVTSunDmg.Value, "{0}")	;Base Sun Damage
		SunAlertOID_S = AddSliderOption("$mslVTsunalertO", mslVTSunAlert.Value, "{2}")	;Sunrise Warning
		SunDawnOID_S = AddSliderOption("$mslVTsundawnO", mslVTSunDawn.Value, "{2}")	;Sunrise Time
		SunDuskOID_S = AddSliderOption("$mslVTsunduskO", mslVTSunDusk.Value, "{2}")	;Sunset Time

		AddEmptyOption()

		AddHeaderOption("$mslVTbloodquality")	;Blood Quality
		BloodRatioOID_S = AddSliderOption("$mslVTbloodratioO", mslVTBloodRatio.Value, "{0}")	;Blood Ratio
		BloodAddictionThresholdOID_S = AddSliderOption("$mslVTbloodaddictionO", mslVTBloodAddictionThreshold.Value, "{0}%")	;Addiction Threshold
		if mslVTSetBloodQuality.Value == 2
			BloodQualityOID_T = AddTextOption("$mslVTbloodqualityO", "$mslVTfull")	;Blood Quality Mode, Full
		elseif mslVTSetBloodQuality.Value == 1
			BloodQualityOID_T = AddTextOption("$mslVTbloodqualityO", "$mslVTpartial")	;Blood Quality Mode, Partial
		else
			BloodQualityOID_T = AddTextOption("$mslVTbloodqualityO", "$mslVTdisabled")	;Blood Quality Mode, Disabled
		endif
		BloodPercentMultOID_S = AddSliderOption("$mslVTbloodpercentO", mslVTBloodPercentMult.Value, "{0}%")	;Preference Gain Ratio

		SetCursorPosition(1) ; Move cursor to top right position

		AddHeaderOption("$mslVTfunctions")		;Functions
		DebConvertOID_T = AddTextOption("$mslVTconvert", "")	;Convert
		DebUpdateOID_T = AddTextOption("$mslVTupdate", "")	;Update
		DebUninstallOID_T = AddTextOption("$mslVTuninstall", "")	;Uninstall

		AddEmptyOption()

		AddHeaderOption("$mslVTappearance")	;Appearance
		if mslVTSetAppearance.Value == 0
			AppearanceOID_T = AddTextOption("$mslVTvisage", "$mslVTvampire")	;Visage, Vampire
		else
			AppearanceOID_T = AddTextOption("$mslVTvisage", "$mslVTnormal")	;Visage, Normal
		endif

	elseIf (page == "$mslVTadvanced")			;Advanced
		;More advanced options that can mess up the game
		SetCursorFillMode(TOP_TO_BOTTOM)
		SetCursorPosition(0)

		AddHeaderOption("$mslVTgeneral")			;General
		KillEssentialOID_B = AddToggleOption("$mslVTkillessentialO", mslVTSetKillEssential.Value)	;Allow Killing Essentials
		CloakOffOID_B = AddToggleOption("$mslVTcloakoffO", mslVTSetCloakOff.Value)	;Emergency Brawl Fix
		if mslVTSetHealMode.Value < 1
			HealModeOID_T = AddTextOption("$mslVThealmodeO", "$mslVT50percent")	;Healing Resistance, 50%
		elseif mslVTSetHealMode.Value == 1
			HealModeOID_T = AddTextOption("$mslVThealmodeO", "$mslVT75percent")	;Healing Resistance, 75%
		elseif mslVTSetHealMode.Value == 2
			HealModeOID_T = AddTextOption("$mslVThealmodeO", "$mslVT100percent")	;Healing Resistance, 100%
		endif
		if mslVTSetRegenMode.Value < 1
			RegenModeOID_T = AddTextOption("$mslVTregenmodeO", "$mslVT50percent")	;Regeneration Resistance, 50%
		elseif mslVTSetRegenMode.Value == 1
			RegenModeOID_T = AddTextOption("$mslVTregenmodeO", "$mslVT75percent")	;Regeneration Resistance, 75%
		elseif mslVTSetRegenMode.Value == 2
			RegenModeOID_T = AddTextOption("$mslVTregenmodeO", "$mslVT100percent")	;Regeneration Resistance, 100%
		endif

		AddEmptyOption()

		AddHeaderOption("$mslVTageing")	;Ageing
		AgeReq1OID_S = AddSliderOption("$mslVTagereq1O", MSLVTConfig.getDaysReqForAge(1), "$mslVT0days")	;Child Stage
		AgeReq2OID_S = AddSliderOption("$mslVTagereq2O", MSLVTConfig.getDaysReqForAge(2), "$mslVT0days")	;Neophyte  Stage
		AgeReq3OID_S = AddSliderOption("$mslVTagereq3O", MSLVTConfig.getDaysReqForAge(3), "$mslVT0days")	;Junior Stage
		AgeReq4OID_S = AddSliderOption("$mslVTagereq4O", MSLVTConfig.getDaysReqForAge(4), "$mslVT0days")	;Bloodkin Stage
		AgeingMultOID_S = AddSliderOption("$mslVTageingmultO", mslVTSetAgeingMult.Value, "{1}")	;Rate Of Ageing
		AgeOID_T = AddTextOption("$mslVTupdateageO", "")	;Update Age

		AddEmptyOption()

		AddHeaderOption("$mslVTexperience")	;Experience
		VLReqBaseOID_S = AddSliderOption("$mslVTvlreqbaseO", mslVTAdvVLReqBase.Value, "{0}")	;VL Base
		VLReqIncrementOID_S = AddSliderOption("$mslVTvlreqincrementO", mslVTAdvVLReqIncrement.Value, "{0}")	;VL Increment
		ExpMultOID_S = AddSliderOption("$mslVTexpmultO", mslVTExpMult.Value, "{1}")	;Experience Multiplier

		SetCursorPosition(1) ; Move cursor to top right position

		AddHeaderOption("$mslVThotkeys")		;Hotkeys
		KeyFeedOID_K = AddKeyMapOption("$mslVTkeyfeedO", mslVTKeyFeed.Value as Int)	;Feed
		KeyStatusOID_K = AddKeyMapOption("$mslVTkeystatusO", mslVTKeyStatus.Value as Int)	;Check Bloodpool
		KeyPSMenuOID_K = AddKeyMapOption("$mslVTkeypsmenuO", mslVTKeyPSMenu.Value as Int)	;Unlock Abilities

	elseIf (page == "$mslVThelptutorials")		;Help & Tutorials
		;Lots of help topics, gameplay explanations and FAQ, especially FAQ, the FAQ to FAQ them all
		SetCursorFillMode(TOP_TO_BOTTOM)
		SetCursorPosition(0)

		AddHeaderOption("$mslVTmodinfo")			;Mod Info
		Info1OID_T = AddTextOption("$mslVTinfo1Q", "")
		Info2OID_T = AddTextOption("$mslVTinfo2Q", "")
		Info3OID_T = AddTextOption("$mslVTinfo3Q", "")
		Info4OID_T = AddTextOption("$mslVTinfo4Q", "")
		Info5OID_T = AddTextOption("$mslVTinfo5Q", "")

		AddEmptyOption()

		AddHeaderOption("$mslVTfaq")			;FAQ
		faq1OID_T = AddTextOption("$mslVTfaq1Q", "")
		faq2OID_T = AddTextOption("$mslVTfaq2Q", "")
		faq3OID_T = AddTextOption("$mslVTfaq3Q", "")
		faq4OID_T = AddTextOption("$mslVTfaq4Q", "")
		faq5OID_T = AddTextOption("$mslVTfaq5Q", "")
		faq6OID_T = AddTextOption("$mslVTfaq6Q", "")
		faq7OID_T = AddTextOption("$mslVTfaq7Q", "")
		faq8OID_T = AddTextOption("$mslVTfaq8Q", "")

		SetCursorPosition(1) ; Move cursor to top right position

		AddHeaderOption("$mslVTtutorials")		;Eat your tutsies, it's healthy

		tut1OID_T = AddTextOption("$mslVTtut1Q", "")
		tut2OID_T = AddTextOption("$mslVTtut2Q", "")
		tut3OID_T = AddTextOption("$mslVTtut3Q", "")
		tut4OID_T = AddTextOption("$mslVTtut4Q", "")
		tut5OID_T = AddTextOption("$mslVTtut5Q", "")
		tut6OID_T = AddTextOption("$mslVTtut6Q", "")
		tut7OID_T = AddTextOption("$mslVTtut7Q", "")
		tut8OID_T = AddTextOption("$mslVTtut8Q", "")
		tut9OID_T = AddTextOption("$mslVTtut9Q", "")
		tut10OID_T = AddTextOption("$mslVTtut10Q", "")
		tut11OID_T = AddTextOption("$mslVTtut11Q", "")
		tut12OID_T = AddTextOption("$mslVTtut12Q", "")
		tut13OID_T = AddTextOption("$mslVTtut13Q", "")
		tut14OID_T = AddTextOption("$mslVTtut14Q", "")
		tut15OID_T = AddTextOption("$mslVTtut15Q", "")
		tut16OID_T = AddTextOption("$mslVTtut16Q", "")
		tut17OID_T = AddTextOption("$mslVTtut17Q", "")
		tut18OID_T = AddTextOption("$mslVTtut18Q", "")

	elseIf (page == "$mslVTcompatibility")		;Compatibility
		;Compatibility options that can be applied without extra plugins
		SetCursorFillMode(TOP_TO_BOTTOM)
		SetCursorPosition(0)

		AddHeaderOption("$mslVTrnd")			;Realistic Needs and Diseases
		comRNDBottlesOID_B = AddToggleOption("$mslVTrndbottlesO", mslVTComRNDBottles.Value)	;Patch Empty Bottles
	endIf
endEvent

event OnOptionHighlight(int option)
	if (option == TurnToAshOID_B)
		SetInfoText("$mslVTturntoashHL")	;If enabled vampires will turn to ash when they die.\nDefault: Enabled
	elseIf (option == DialSpeechOID_B)
		SetInfoText("$mslVTdialspeechHL")	;Set whether successfully passing feeding dialogue checks increases Speech skill.\nDefault: Enabled
	elseIf (option == DeathSceneOID_B)
		SetInfoText("$mslVTdeathsceneHL")	;Enable or disable death animations during player's transformation scene.\nDisable if the animations are causing any problems.\nDefault: Enabled
	elseIf (option == NoBiteControlsOID_B)
		SetInfoText("$mslVTnobitecontrolHL")	;If enabled player will be prevented from feeding if activation or combat controls are disabled.\nEnabling it  helps immersion and compatibility with mods like Sanguine Debauchery.\nDefault: Enabled
	elseIf (option == DialDifficultyOID_S)
		SetInfoText("$mslVTdialdifficultyHL")	;Adjust the overall difficulty of Speech checks for dialogue feeding.\nHigher values make passing the checks harder.\nDefault: 0
	elseIf (option == FeedDelayOID_S)
		SetInfoText("$mslVTfeeddelayHL")	;Adjust how long script should wait for the animation to finish before victim dies  or loses consciousness.\nDeath or paralysis should happen exactly at the end of feeding animation.\nDefault: 0.35
	elseIf (option == SunDamageOID_S)
		SetInfoText("$mslVTsundamageHL")	;Set the base sun damage per second before environmental factors are taken into account.\nSetting to 0  disables sun damage completely.\nDefault: 10
	elseIf (option == SunAlertOID_S)
		SetInfoText("$mslVTsunalertHL")	;Set when player receives a warning that sunrise is coming soon. Set to 0 to disable.\nThese values equal hours on a 24 hour clock. Use fractions of 1.0 to specify minutes (for example 6:30 = 6.50)\nDefault: 5.50
	elseIf (option == SunDawnOID_S)
		SetInfoText("$mslVTsundawnHL")	;Set when sun damage begins. It should occur near sunrise.\nThese values equal hours on a 24 hour clock. Use fractions of 1.0 to specify minutes (for example 6:30 = 6.50)\nDefault: 6.00, Vanilla: 5.00
	elseIf (option == SunDuskOID_S)
		SetInfoText("$mslVTsunduskHL")	;Set when sun damage ends. It should occur near sunset.\nThese values equal hours on a 24 hour clock. Use fractions of 1.0 to specify minutes (for example 6:30 = 6.50)\nDefault: 19.00, Vanilla: 19.00
	elseIf (option == BloodRatioOID_S)
		SetInfoText("$mslVTbloodratioHL")	;Set the base of how many blood points you gain per health point stolen from your victim\nNew default: 16, Old default: 8, Original  default: 4
	elseIf (option == BloodAddictionThresholdOID_S)
		SetInfoText("$mslVTbloodaddictionHL")	;Preference level needed to trigger addiction after feeding from a given person.\nDefault: 30%
	elseIf (option == BloodQualityOID_T)
		SetInfoText("$mslVTbloodqualityHL")	;Disabled: Disables all Blood Quality features.\nPartial: Only enables blood preference.\nFull: Enables both blood preference and addiction.
	elseIf (option == BloodPercentMultOID_S)
		SetInfoText("$mslVTbloodpercentHL")	;Drinking 100% of a person's blood equals % preference increase for that person.\nDefault: 5%
	elseIf (option == DebConvertOID_T)
		SetInfoText("$mslVTdebconvertHL")	;Run this to convert a vanilla vampire into Vampiric Thirst vampire or to turn a mortal character into a vampire.
	elseIf (option == DebUpdateOID_T)
		SetInfoText("$mslVTdebupdateHL")	;Use if updating VT to a newer version.\nThis is also your panic button: use whenever something isn't working like it should!
	elseIf (option == DebUninstallOID_T)
		SetInfoText("$mslVTdebuninstallHL")	;Use this to uninstall Vampiric Thirst and remove whatever traces it can from your game.\nWarning! To do this  it must cure player's vampirism too!
	elseIf (option == AppearanceOID_T)
		SetInfoText("$mslVTvisageHL")	;Choose whether you want to have the feral look of a vampire or be indistinguishable from mortals.\nIt will take effect immediately if player is a vampire or wait for player to become a vampire if not.\nThis choice is aesthetic and has no effect on gameplay.
	elseIf (option == KillEssentialOID_B)
		SetInfoText("$mslVTkillessentialHL")	;Checking this will allow draining essential characters to death.\nONLY USE IF YOU KNOW WHAT YOU'RE DOING!\nDefault: Disabled
	elseIf (option == CloakOffOID_B)
		SetInfoText("$mslVTcloakoffHL")	;Enable this if experiencing issues with brawls\nUSE ONLY FOR THE DURATION OF BRAWL AND DISABLE AFTERWARDS!!!\nDefault: Disabled
	elseIf (option == AgeReq1OID_S)
		SetInfoText("$mslVTagereq1HL")	;Set how many days since becoming a vampire must pass before advancing to the Child stage.\nDefault: 7
	elseIf (option == AgeReq2OID_S)
		SetInfoText("$mslVTagereq2HL")	;Set how many days since becoming a vampire must pass before advancing to the Neophyte stage.\nDefault: 21
	elseIf (option == AgeReq3OID_S)
		SetInfoText("$mslVTagereq3HL")	;Set how many days since becoming a vampire must pass before advancing to the Junior stage.\nDefault: 90
	elseIf (option == AgeReq4OID_S)
		SetInfoText("$mslVTagereq4HL")	;Set how many days since becoming a vampire must pass before advancing to the Bloodkin stage.\nDefault: 270
	elseIf (option == AgeingMultOID_S)
		SetInfoText("$mslVTageingmultHL")	;Set the overall scale of how fast vampire ageing progresses.\nHigher values mean faster ageing.\nDefault: 1.0
	elseIf (option == AgeOID_T)
		SetInfoText("$mslVTupdateageHL")	;Click this to update your age if you modified any of the above values.
	elseIf (option == VLReqBaseOID_S)
		SetInfoText("$mslVTvlreqbaseHL")	;Requirements for advancing through Vampire Lord perk tree are calculated according to:\nNext perk = Base + (Total number of perks earned * Increment)\nDefault: 14, Vanilla: 5
	elseIf (option == VLReqIncrementOID_S)
		SetInfoText("$mslVTvlreqincrementHL")	;Requirements for advancing through Vampire Lord perk tree are calculated according to:\nNext perk = Base + (Total number of perks earned * Increment)\nDefault: 7, Vanilla: 2
	elseIf (option == ExpMultOID_S)
		SetInfoText("$mslVTexpmultHL")	;Multiplier of all Blood Experience gained by feeding and using abilities.\nDefault: 0.7
	elseIf (option == KeyFeedOID_K)
		SetInfoText("$mslVTkeyfeedHL")	;The hotkey used to initiate contextual feeding interactions.\nDefault: V
	elseIf (option == KeyStatusOID_K)
		SetInfoText("$mslVTkeystatusHL")	;The hotkey to display how hungry player character is.\nDefault: B
	elseIf (option == KeyPSMenuOID_K)
		SetInfoText("$mslVTkeypsmenuHL")	;Pressing this hotkey displays a menu of powers you can buy or upgrade.\nDefault: H
	elseIf (option == comRNDBottlesOID_B)
		SetInfoText("$mslVTrndbottlesHL")	;Allow VT to detect and use empty bottles from Realistic Needs and Diseases.
	elseIf (option == HealModeOID_T)
		SetInfoText("$mslVThealmodeHL")	;Choose how ineffective conventional (not necromantic) healing magic is on vampires.\nThe percentage will be subtracted from healing spells, enchantments and potions.\nVampiric Thirst default: 50%. Requiem default: 100% (not recommended)
	elseIf (option == RegenModeOID_T)
		SetInfoText("$mslVTregenmodeHL")	;Choose how ineffective conventional (not necromantic) health regeneration magic is on vampires.\nThe percentage will be subtracted from health regeneration spells, enchantments and potions.\nVampiric Thirst default: 50%
	endIf
endEvent

Function Toggle(GlobalVariable GlobalVar)
	if GlobalVar.Value == 0
		GlobalVar.Value = 1
	else
		GlobalVar.Value = 0
	endif
EndFunction

event OnOptionSelect(int option)
	if (option == TurnToAshOID_B)
 		Toggle(mslVTSetTurnToAsh)
		SetToggleOptionValue(TurnToAshOID_B, mslVTSetTurnToAsh .Value)
	;;;
	elseif option == reinitOID_S
		MSLVTConfig.autoupdate()
	elseif option == btn_dumpPlr
		JValue.writeToFile(JFormDB.findEntry("mslVT", Game.GetPlayer()), JContainers.userDirectory() + "mslVT/playerDump.json")
	elseif option == btn_readPlr
		int jEntry = JValue.readFromFile(JContainers.userDirectory() + "mslVT/playerDump.json")
		if jEntry
			JFormDB.setEntry("mslVT", Game.GetPlayer(), jEntry)
		endif
	;;;
	elseif (option == DialSpeechOID_B)
 		Toggle(mslVTSetDialSpeech)
		SetToggleOptionValue(DialSpeechOID_B, mslVTSetDialSpeech.Value)
	elseif (option == DeathSceneOID_B)
 		Toggle(mslVTSetDeathSceneAnim)
		SetToggleOptionValue(DeathSceneOID_B, mslVTSetDeathSceneAnim.Value)
	elseif (option == NoBiteControlsOID_B)
 		Toggle(mslVTSetNoBiteControls)
		SetToggleOptionValue(NoBiteControlsOID_B, mslVTSetNoBiteControls.Value)
	elseif (option == BloodQualityOID_T)
		if mslVTSetBloodQuality.Value == 1
			SetTextOptionValue(BloodQualityOID_T, "$mslVTfull")	;Full
			mslVTSetBloodQuality.Value = 2
		elseif mslVTSetBloodQuality.Value == 0
			SetTextOptionValue(BloodQualityOID_T, "$mslVTpartial")	;Partial
			mslVTSetBloodQuality.Value = 1
		else
			SetTextOptionValue(BloodQualityOID_T, "$mslVTdisabled")	;Disabled
			mslVTSetBloodQuality.Value = 0
		endif
	elseif (option == DebConvertOID_T)
		ShowMessage("$mslVTdebexitmenu", false)	;Selected procedure will start once you exit menu.
		mslVTMCMQST.DebugConvert()
	elseif (option == DebUpdateOID_T)
		ShowMessage("$mslVTdebexitmenu", false)	;Selected procedure will start once you exit menu.
		mslVTMCMQST.DebugUpdate()
	elseif (option == DebUninstallOID_T)
		ShowMessage("$mslVTdebexitmenu", false)	;Selected procedure will start once you exit menu.
		mslVTMCMQST.DebugUninstall()
	elseif (option == AppearanceOID_T)
		if mslVTSetAppearance.Value == 0
			mslVTSetAppearance.Value = 1
			SetTextOptionValue(AppearanceOID_T, "$mslVTnormal")	;Normal
		else
			mslVTSetAppearance.Value = 0
			SetTextOptionValue(AppearanceOID_T, "$mslVTvampire")	;Vampire
		endif
		PlayerVampireQuest.ToggleAppearance(mslVTSetAppearance.Value as Int, game.GetPlayer())
	elseif (option == KillEssentialOID_B)
 		Toggle(mslVTSetKillEssential)
		SetToggleOptionValue(KillEssentialOID_B, mslVTSetKillEssential.Value)
	elseif (option == CloakOffOID_B)
 		Toggle(mslVTSetCloakOff)
		SetToggleOptionValue(CloakOffOID_B, mslVTSetCloakOff.Value)
	elseif (option == AgeOID_T)
		ShowMessage("$mslVTdebexitmenu", false)	;Selected procedure will start once you exit menu.
		mslVTAge.Value = 0
	elseif (option == Info1OID_T)
		ShowMessage("$mslVTinfo1A", false)
	elseif (option == Info2OID_T)
		ShowMessage("$mslVTinfo2A", false)
	elseif (option == Info3OID_T)
		ShowMessage("$mslVTinfo3A", false)
	elseif (option == Info4OID_T)
		ShowMessage("$mslVTinfo4A", false)
	elseif (option == Info5OID_T)
		ShowMessage("$mslVTinfo5A", false)
	elseif (option == faq1OID_T)
		ShowMessage("$mslVTfaq1A", false)
	elseif (option == faq2OID_T)
		ShowMessage("$mslVTfaq2A", false)
	elseif (option == faq3OID_T)
		ShowMessage("$mslVTfaq3A", false)
	elseif (option == faq4OID_T)
		ShowMessage("$mslVTfaq4A", false)
	elseif (option == faq5OID_T)
		ShowMessage("$mslVTfaq5A", false)
	elseif (option == faq6OID_T)
		ShowMessage("$mslVTfaq6A", false)
	elseif (option == faq7OID_T)
		ShowMessage("$mslVTfaq7A", false)
	elseif (option == faq8OID_T)
		ShowMessage("$mslVTfaq8A", false)
	elseif (option == tut1OID_T)
		ShowMessage("$mslVTtut1A", false)
	elseif (option == tut2OID_T)
		ShowMessage("$mslVTtut2A", false)
	elseif (option == tut3OID_T)
		ShowMessage("$mslVTtut3A", false)
	elseif (option == tut4OID_T)
		ShowMessage("$mslVTtut4A", false)
	elseif (option == tut5OID_T)
		ShowMessage("$mslVTtut5A", false)
	elseif (option == tut6OID_T)
		ShowMessage("$mslVTtut6A", false)
	elseif (option == tut7OID_T)
		ShowMessage("$mslVTtut7A", false)
	elseif (option == tut8OID_T)
		ShowMessage("$mslVTtut8A", false)
	elseif (option == tut9OID_T)
		ShowMessage("$mslVTtut9A", false)
	elseif (option == tut10OID_T)
		ShowMessage("$mslVTtut10A", false)
	elseif (option == tut11OID_T)
		ShowMessage("$mslVTtut11A", false)
	elseif (option == tut12OID_T)
		ShowMessage("$mslVTtut12A", false)
	elseif (option == tut13OID_T)
		ShowMessage("$mslVTtut13A", false)
	elseif (option == tut14OID_T)
		ShowMessage("$mslVTtut14A", false)
	elseif (option == tut15OID_T)
		ShowMessage("$mslVTtut15A", false)
	elseif (option == tut16OID_T)
		ShowMessage("$mslVTtut16A", false)
	elseif (option == tut17OID_T)
		ShowMessage("$mslVTtut17A", false)
	elseif (option == tut18OID_T)
		ShowMessage("$mslVTtut18A", false)
	elseif (option == comRNDBottlesOID_B)
 		Toggle(mslVTComRNDBottles)
		mslVTMCMQST.ApplyPatch(mslVTComRNDBottles)
		SetToggleOptionValue(comRNDBottlesOID_B, mslVTComRNDBottles.Value)
	elseif (option == HealModeOID_T)
		if mslVTSetHealMode.Value == 1
			SetTextOptionValue(HealModeOID_T, "$mslVT100percent")	;100%
			mslVTSetHealMode.Value = 2
		elseif mslVTSetHealMode.Value == 0
			SetTextOptionValue(HealModeOID_T, "$mslVT75percent")	;75%
			mslVTSetHealMode.Value = 1
		else
			SetTextOptionValue(HealModeOID_T, "$mslVT50percent")	;50%
			mslVTSetHealMode.Value = 0
		endif
	elseif (option == RegenModeOID_T)
		if mslVTSetRegenMode.Value == 1
			SetTextOptionValue(RegenModeOID_T, "$mslVT100percent")	;100%
			mslVTSetRegenMode.Value = 2
		elseif mslVTSetRegenMode.Value == 0
			SetTextOptionValue(RegenModeOID_T, "$mslVT75percent")	;75%
			mslVTSetRegenMode.Value = 1
		else
			SetTextOptionValue(RegenModeOID_T, "$mslVT50percent")	;50%
			mslVTSetRegenMode.Value = 0
		endif
	endIf
endEvent

event OnOptionDefault(int option)
	if (option == TurnToAshOID_B)
		mslVTSetTurnToAsh.Value = 1
		SetToggleOptionValue(TurnToAshOID_B, mslVTSetTurnToAsh.Value)
	elseIf (option == DialSpeechOID_B)
		mslVTSetDialSpeech.Value = 1
		SetToggleOptionValue(DialSpeechOID_B, mslVTSetDialSpeech.Value)
	elseIf (option == DeathSceneOID_B)
		mslVTSetDeathSceneAnim.Value = 1
		SetToggleOptionValue(DeathSceneOID_B, mslVTSetDeathSceneAnim.Value)
	elseIf (option == NoBiteControlsOID_B)
		mslVTSetNoBiteControls.Value = 1
		SetToggleOptionValue(NoBiteControlsOID_B, mslVTSetNoBiteControls.Value)
	elseIf (option == BloodQualityOID_T)
		mslVTSetBloodQuality.Value = 2
		SetTextOptionValue(BloodQualityOID_T, "$mslVTfull")	;Full
	elseIf (option == KillEssentialOID_B)
		mslVTSetKillEssential.Value = 0
		SetToggleOptionValue(KillEssentialOID_B, mslVTSetKillEssential.Value)
	elseIf (option == CloakOffOID_B)
		mslVTSetCloakOff.Value = 0
		SetToggleOptionValue(CloakOffOID_B, mslVTSetCloakOff.Value)
	elseIf (option == HealModeOID_T)
		mslVTSetHealMode.Value = 0
		SetTextOptionValue(HealModeOID_T, "$mslVT50percent")	;50%
	elseIf (option == RegenModeOID_T)
		mslVTSetRegenMode.Value = 0
		SetTextOptionValue(RegenModeOID_T, "$mslVT50percent")	;50%
	endIf
endEvent

event OnOptionSliderOpen(int option)
	if (option == DialDifficultyOID_S)
		SetSliderDialogStartValue(mslVTFDDifficulty.Value)
		SetSliderDialogDefaultValue(0.0)
		SetSliderDialogRange(-100.0, 100.0)
		SetSliderDialogInterval(1.0)
	elseif (option == FeedDelayOID_S)
		SetSliderDialogStartValue(mslVTFeedDelay.Value)
		SetSliderDialogDefaultValue(0.35)
		SetSliderDialogRange(0.0, 2.0)
		SetSliderDialogInterval(0.01)
	elseif (option == SunDamageOID_S)
		SetSliderDialogStartValue(mslVTSunDmg.Value)
		SetSliderDialogDefaultValue(10.0)
		SetSliderDialogRange(0.0, 100.0)
		SetSliderDialogInterval(1.0)
	elseif (option == SunAlertOID_S)
		SetSliderDialogStartValue(mslVTSunAlert.Value)
		SetSliderDialogDefaultValue(5.5)
		SetSliderDialogRange(0.0, 24.0)
		SetSliderDialogInterval(0.05)
	elseif (option == SunDawnOID_S)
		SetSliderDialogStartValue(mslVTSunDawn.Value)
		SetSliderDialogDefaultValue(6.0)
		SetSliderDialogRange(0.0, 24.0)
		SetSliderDialogInterval(0.05)
	elseif (option == SunDuskOID_S)
		SetSliderDialogStartValue(mslVTSunDusk.Value)
		SetSliderDialogDefaultValue(19.0)
		SetSliderDialogRange(0.0, 24.0)
		SetSliderDialogInterval(0.05)
	elseif (option == BloodRatioOID_S)
		SetSliderDialogStartValue(mslVTBloodRatio.Value)
		SetSliderDialogDefaultValue(16.0)
		SetSliderDialogRange(1.0, 100.0)
		SetSliderDialogInterval(1.0)
	elseif (option == BloodAddictionThresholdOID_S)
		SetSliderDialogStartValue(mslVTBloodAddictionThreshold.Value)
		SetSliderDialogDefaultValue(30.0)
		SetSliderDialogRange(0.0, 100.0)
		SetSliderDialogInterval(1.0)
	elseif (option == BloodPercentMultOID_S)
		SetSliderDialogStartValue(mslVTBloodPercentMult.Value)
		SetSliderDialogDefaultValue(5.0)
		SetSliderDialogRange(0.0, 100.0)
		SetSliderDialogInterval(1.0)
	elseif (option == AgeReq1OID_S)
		SetSliderDialogStartValue(MSLVTConfig.getDaysReqForAge(1))
		SetSliderDialogDefaultValue(7.0)
		SetSliderDialogRange(0.0, 500.0)
		SetSliderDialogInterval(1.0)
	elseif (option == AgeReq2OID_S)
		SetSliderDialogStartValue(MSLVTConfig.getDaysReqForAge(2))
		SetSliderDialogDefaultValue(21.0)
		SetSliderDialogRange(0.0, 500.0)
		SetSliderDialogInterval(1.0)
	elseif (option == AgeReq3OID_S)
		SetSliderDialogStartValue(MSLVTConfig.getDaysReqForAge(3))
		SetSliderDialogDefaultValue(90.0)
		SetSliderDialogRange(0.0, 500.0)
		SetSliderDialogInterval(1.0)
	elseif (option == AgeReq4OID_S)
		SetSliderDialogStartValue(MSLVTConfig.getDaysReqForAge(4))
		SetSliderDialogDefaultValue(270.0)
		SetSliderDialogRange(0.0, 500.0)
		SetSliderDialogInterval(1.0)
	elseif (option == AgeingMultOID_S)
		SetSliderDialogStartValue(mslVTSetAgeingMult.Value)
		SetSliderDialogDefaultValue(1.0)
		SetSliderDialogRange(0.1, 10.0)
		SetSliderDialogInterval(0.1)
	elseif (option == VLReqBaseOID_S)
		SetSliderDialogStartValue(mslVTAdvVLReqBase.Value)
		SetSliderDialogDefaultValue(14.0)
		SetSliderDialogRange(1.0, 100.0)
		SetSliderDialogInterval(1.0)
	elseif (option == VLReqIncrementOID_S)
		SetSliderDialogStartValue(mslVTAdvVLReqIncrement.Value)
		SetSliderDialogDefaultValue(7.0)
		SetSliderDialogRange(1.0, 100.0)
		SetSliderDialogInterval(1.0)
	elseif (option == ExpMultOID_S)
		SetSliderDialogStartValue(mslVTExpMult.Value)
		SetSliderDialogDefaultValue(0.7)
		SetSliderDialogRange(0.1, 10.0)
		SetSliderDialogInterval(0.1)
	endIf
endEvent

event OnOptionSliderAccept(int option, float value)
	if (option == DialDifficultyOID_S)
		mslVTFDDifficulty.Value = value
		SetSliderOptionValue(DialDifficultyOID_S, mslVTFDDifficulty.Value, "{0}")
	elseif (option == FeedDelayOID_S)
		mslVTFeedDelay.Value = value
		SetSliderOptionValue(FeedDelayOID_S, mslVTFeedDelay.Value, "{2}")
	elseif (option == SunDamageOID_S)
		mslVTSunDmg.Value = value
		SetSliderOptionValue(SunDamageOID_S, mslVTSunDmg.Value, "{0}")
	elseif (option == SunAlertOID_S)
		mslVTSunAlert.Value = value
		SetSliderOptionValue(SunAlertOID_S, mslVTSunAlert.Value, "{2}")
	elseif (option == SunDawnOID_S)
		mslVTSunDawn.Value = value
		SetSliderOptionValue(SunDawnOID_S, mslVTSunDawn.Value, "{2}")
	elseif (option == SunDuskOID_S)
		mslVTSunDusk.Value = value
		SetSliderOptionValue(SunDuskOID_S, mslVTSunDusk.Value, "{2}")
	elseif (option == BloodRatioOID_S)
		mslVTBloodRatio.Value = value
		SetSliderOptionValue(BloodRatioOID_S, mslVTBloodRatio.Value, "{0}")
	elseif (option == BloodAddictionThresholdOID_S)
		mslVTBloodAddictionThreshold.Value = value
		SetSliderOptionValue(BloodAddictionThresholdOID_S, mslVTBloodAddictionThreshold.Value, "{0}%")
	elseif (option == BloodPercentMultOID_S)
		mslVTBloodPercentMult.Value = value
		SetSliderOptionValue(BloodPercentMultOID_S, mslVTBloodPercentMult.Value, "{0}%")
	elseif (option == AgeReq1OID_S)
		MSLVTConfig.setDaysReqForAge(value, 1)
		SetSliderOptionValue(AgeReq1OID_S, MSLVTConfig.getDaysReqForAge(1), "$mslVT0days")	;{0} days
	elseif (option == AgeReq2OID_S)
		MSLVTConfig.setDaysReqForAge(value, 2)
		SetSliderOptionValue(AgeReq2OID_S, MSLVTConfig.getDaysReqForAge(2), "$mslVT0days")	;{0} days
	elseif (option == AgeReq3OID_S)
		MSLVTConfig.setDaysReqForAge(value, 3)
		SetSliderOptionValue(AgeReq3OID_S, MSLVTConfig.getDaysReqForAge(3), "$mslVT0days")	;{0} days
	elseif (option == AgeReq4OID_S)
		MSLVTConfig.setDaysReqForAge(value, 4)
		SetSliderOptionValue(AgeReq4OID_S, MSLVTConfig.getDaysReqForAge(4), "$mslVT0days")	;{0} days
	elseif (option == AgeingMultOID_S)
		mslVTSetAgeingMult.Value = value
		SetSliderOptionValue(AgeingMultOID_S, mslVTSetAgeingMult.Value, "{1}")
	elseif (option == VLReqBaseOID_S)
		mslVTAdvVLReqBase.Value = value
		SetSliderOptionValue(VLReqBaseOID_S, mslVTAdvVLReqBase.Value, "{0}")
	elseif (option == VLReqIncrementOID_S)
		mslVTAdvVLReqIncrement.Value = value
		SetSliderOptionValue(VLReqIncrementOID_S, mslVTAdvVLReqIncrement.Value, "{0}")
	elseif (option == ExpMultOID_S)
		mslVTExpMult.Value = value
		SetSliderOptionValue(ExpMultOID_S, mslVTExpMult.Value, "{1}")
	endIf
endEvent

Function AssignKey(globalvariable hotKey, int option, int keyCode, string conflictControl, string conflictName)
		bool continue = true
		if (conflictControl != "")
			string msg
			if (conflictName != "")
				msg = "This key is already mapped to:\n" + conflictControl + "\n(" + conflictName + ")\n\nAre you sure you want to continue?"
			else
				msg = "This key is already mapped to:\n" + conflictControl + "\n\nAre you sure you want to continue?"
			endIf

			continue = ShowMessage(msg, true, "$Yes", "$No")
		endIf
		if (continue)
			hotKey.Value = keyCode
			SetKeymapOptionValue(option, keyCode)
			mslVTHotkeysQST.RegisterHotkeys()
		endIf
EndFunction

string Function GetCustomControl(int keyCode)
	if (keyCode == mslVTKeyFeed.Value)
		return "Feed"
	elseif (keyCode == mslVTKeyStatus.Value)
		return "Check Bloodpool"
	elseif (keyCode == mslVTKeyPSMenu.Value)
		return "Unlock Abilities"
	else
		return ""
	endIf
endFunction

Event OnOptionKeyMapChange(int option, int keyCode, string conflictControl, string conflictName)
	if (option == KeyFeedOID_K)
			AssignKey(mslVTKeyFeed, option, keyCode, conflictControl, conflictName)
	elseif (option == KeyStatusOID_K)
			AssignKey(mslVTKeyStatus, option, keyCode, conflictControl, conflictName)
	elseif (option == KeyPSMenuOID_K)
			AssignKey(mslVTKeyPSMenu, option, keyCode, conflictControl, conflictName)
	endIf
endEvent

mslVTMCMDebugSCR Property mslVTMCMQST  Auto  
mslVTHotkeysSCR Property mslVTHotkeysQST  Auto  
PlayerVampireQuestScript Property PlayerVampireQuest  Auto  