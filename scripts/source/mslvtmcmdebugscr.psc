Scriptname mslVTMCMDebugSCR extends Quest  

Function DebugConvert()
	if Wait == True
		DebWaitMSG.Show()
	else
		int button = DebConMSG.Show()
		if button == 0; Yes
			RunConvert()
		endif
	endif
EndFunction

Function RunConvert()
	Wait = TRUE
	PlayerVampireQuest.Stop()
	PlayerVampireQuest.Start()

	if !PlayerVampireQuest.AuthenticateScript()
		DebAuthErrorMSG.Show()
		Wait = FALSE
		return
	endif

	Game.GetPlayer().RemoveSpell(VampireVampirism)
	Game.GetPlayer().RemoveSpell(VampirePoisonResist)
	Game.GetPlayer().RemoveSpell(VampireHuntersSight)

	int Index = 0
	while (Index < SpellsVanilla.GetSize())
		Game.GetPlayer().RemoveSpell(SpellsVanilla.GetAt(Index) as Spell)
		Index += 1
	endwhile
	PlayerVampireQuest.VampireChange(Game.GetPlayer())

;	Game.GetPlayer().AddSpell(VampireVampirism)
;	Game.GetPlayer().AddSpell(VampirePoisonResist)
;	Game.GetPlayer().AddSpell(VampireHuntersSight)

	if mslVTExp.Value < Game.QueryStat("Days as a Vampire")*Game.QueryStat("Necks Bitten")*25
		mslVTExp.Value = Game.QueryStat("Days as a Vampire")*Game.QueryStat("Necks Bitten")*25
	endif
	DebDoneMSG.Show()
	SendModEvent("EndVTConvert")
	Wait = FALSE
EndFunction

Function DebugUpdate()
	if Wait == True
		DebWaitMSG.Show()
	else
		int button = DebUpdMSG.Show()
		if button == 0; Yes
			RunUpdate()
		endif
	endif
EndFunction

Function RunUpdate()
	Wait  = TRUE
	PlayerVampireQuest.Stop()
	PlayerVampireQuest.Start()
	if !PlayerVampireQuest.AuthenticateScript()
		DebAuthErrorMSG.Show()
		Wait = FALSE
		return
	endif
	PlayerVampireQuest.StartVTUpdate()
	mslVTHotkeysQST.Stop()
	mslVTHotkeysQST.Start()
	mslVTNPCPowersQST.Stop()
	mslVTNPCPowersQST.Start()
	mslVTSetNPCPowers.SetValue(0)
	mslVTSetNPCPowers.SetValue(1)
	DebDoneMSG.Show()
	SendModEvent("EndVTUpdate")
	Wait = FALSE
EndFunction

Function DebugUninstall()
	if Wait == True
		DebWaitMSG.Show()
	else
		int button = DebUniMSG.Show()
		if button == 0; Yes
			RunUninstall()
		endif
	endif
EndFunction

Function RunUninstall()
	Wait = TRUE
	if !PlayerVampireQuest.AuthenticateScript()
		DebAuthErrorMSG.Show()
		Wait = FALSE
		return
	endif
	Game.GetPlayer().RemoveSpell(DLC1VampireChange)
	PlayerVampireQuest.VampireCure(Game.Getplayer())
	mslVTNPCPowersQST.Stop()
	mslVTSetNPCPowers.SetValue(0)
	PlayerVampireQuest.Stop()
	PlayerVampireQuest.Start()
	mslVTNPCPowersQST.Stop()
	DebDoneMSG.Show()
	SendModEvent("EndVTUninstall")
	Wait = FALSE
EndFunction

Function ApplyPatch(GlobalVariable Option)
	If Option == mslVTComRNDBottles
		string rndMod = "RealisticNeedsandDiseases.esp"

		if Game.GetModByName(rndMod)
			EmptyBottles.RemoveAddedForm(Game.GetFormFromFile(0x0043b0, rndMod))
			EmptyBottles.RemoveAddedForm(Game.GetFormFromFile(0x0043b2, rndMod))
			EmptyBottles.RemoveAddedForm(Game.GetFormFromFile(0x0043b4, rndMod))
			if mslVTComRNDBottles.Value != 0.0
				EmptyBottles.AddForm(Game.GetFormFromFile(0x0043b0, rndMod))
				EmptyBottles.AddForm(Game.GetFormFromFile(0x0043b2, rndMod))
				EmptyBottles.AddForm(Game.GetFormFromFile(0x0043b4, rndMod))
				;Debug.MessageBox("Size:" + EmptyBottles.GetSize() + "Forms:" + Game.GetFormFromFile(0x0043b4, "RealisticNeedsandDiseases.esp") + Game.GetFormFromFile(0x0043b2, "RealisticNeedsandDiseases.esp") + Game.GetFormFromFile(0x0043b0, "RealisticNeedsandDiseases.esp"))
			endif
		endif
	Endif
EndFunction

bool Function WearsItemWithKeyword_Safe(actor akVampire, string kwName) global
	Keyword kw = Keyword.GetKeyword(kwName)
	return kw && akVampire.WornHasKeyword(kw)
EndFunction

bool Function CheckNoBite(actor akVampire)
	If mslVTSetNoBiteControls.Value == 1 && (akVampire.GetWornForm(0x00000001) as Armor || Game.IsFightingControlsEnabled() == 0 ||\
	  Game.IsActivateControlsEnabled() == 0 || WearsItemWithKeyword_Safe(akVampire, "mslVTNoBiteWearKW") ||\
	  WearsItemWithKeyword_Safe(akVampire, "zbfWornGag") || WearsItemWithKeyword_Safe(akVampire, "_SD_bound_gag") ||\
	  WearsItemWithKeyword_Safe(akVampire, "zad_DeviousGag"))
		mslVTFeedNoBiteControlsMSG.Show()
		return true
	Endif

	return false
EndFunction

GlobalVariable Property mslVTComRNDBottles Auto
Formlist Property EmptyBottles Auto

Bool Property Wait  Auto
Message Property DebConMSG  Auto  
Message Property DebUpdMSG  Auto  
Message Property DebUniMSG  Auto  
PlayerVampireQuestScript Property PlayerVampireQuest  Auto  
Message Property DebDoneMSG  Auto  
Message Property DebWaitMSG  Auto  
SPELL Property DLC1VampireChange  Auto  
Quest Property mslVTNPCPowersQST  Auto  
GlobalVariable Property mslVTSetNPCPowers  Auto  
Spell Property VampireVampirism Auto
Spell Property VampirePoisonResist Auto
Spell Property VampireHuntersSight Auto
GlobalVariable Property mslVTExp Auto
Formlist Property SpellsVanilla Auto
Quest Property mslVTHotkeysQST  Auto
Message Property DebAuthErrorMSG Auto

GlobalVariable Property mslVTSetNoBiteControls Auto
Message Property mslVTFeedNoBiteControlsMSG Auto