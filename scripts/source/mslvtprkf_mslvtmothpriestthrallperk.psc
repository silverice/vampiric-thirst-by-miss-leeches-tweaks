;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 7
Scriptname mslVTPRKF_mslVTMothPriestThrallPERK Extends Perk Hidden

;BEGIN FRAGMENT Fragment_2
Function Fragment_2(ObjectReference akTargetRef, Actor akActor)
;BEGIN CODE
mslVTPwSubMesASeductionDexion.Cast(akActor, akTargetRef)
;DLC1VQ03Vampire.SetStage(68)
;DLC1VQ03Vampire.SetStage(70)
akActor.RemoveSpell(mslVTPwSubMesASeductionDexion)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

SPELL Property mslVTPwSubMesASeduction  Auto  

Quest Property DLC1VQ03Vampire  Auto  

SPELL Property mslVTPwSubMesASeductionDexion  Auto  
