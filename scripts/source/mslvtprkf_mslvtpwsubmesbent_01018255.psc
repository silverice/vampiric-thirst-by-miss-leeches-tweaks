;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 11
Scriptname mslVTPRKF_mslVTPwSubMesBEnt_01018255 Extends Perk Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akTargetRef, Actor akActor)
;BEGIN CODE
CostTotal = CostBase*mslVTCostMultINS.GetValue()
If MSLVTVampire.getBlood(akActor) < CostTotal
	;Debug.Notification("The power sparked and fizzled miserably. I am too hungry to maintain it right now!")
	PowerFailMSG.Show()
else
	MSLVTVampire.modBlood(akActor, -CostTotal)
	mslVTExp.Mod(CostBase/2*mslVTExpMult.GetValue())
	(akTargetRef as Actor).AddToFaction(EntranceFaction)
Endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

SPELL Property EntrancementSP  Auto  

GlobalVariable Property Cost  Auto  

GlobalVariable Property mslVTCostMultINS  Auto  

GlobalVariable Property mslVTExp  Auto  

Message Property PowerFailMSG Auto

Float Property CostBase Auto

Float Property CostTotal Auto

GlobalVariable Property mslVTExpMult  Auto  

Faction Property EntranceFaction  Auto  
