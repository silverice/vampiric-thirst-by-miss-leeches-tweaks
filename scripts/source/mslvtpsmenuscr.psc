Scriptname mslVTPSMenuSCR extends ActiveMagicEffect  

Event OnEffectStart(Actor akTarget, Actor akCaster)

	bool Done = FALSE
	while (Done == FALSE)
		;Debug.Notification("Main menu")
		int buttonMain = MainMSG.Show(mslVTExpPoints.GetValueInt())
		if buttonMain == 3
			Done = TRUE
		else
			;Debug.Notification("Category menu")
			CatSPFL = MainSPFL.GetAt(buttonMain) as Formlist
			PowerCatFL = PowerMainFL.GetAt(buttonMain) as Formlist
			CatMSG = MainMSGFL.GetAt(buttonMain) as Message
			int buttonCat = CatMSG.Show()
			if buttonCat == 4
				Done = TRUE
			elseif buttonCat == 3
				;return to main menu
			else
				;Debug.Notification("Subcategory menu")
				PowerSubCatFL = PowerCatFL.GetAt(buttonCat) as Formlist
				SubCatSPFL = CatSPFL.GetAt(buttonCat) as Formlist
				if CatMSG == MainMSGFL.GetAt(0)
					SubCatMSG = SubCatSubMSGFL.GetAt(buttonCat) as Message
				elseif CatMSG == MainMSGFL.GetAt(1)
					SubCatMSG = SubCatMigMSGFL.GetAt(buttonCat) as Message
				elseif CatMSG == MainMSGFL.GetAt(2)
					SubCatMSG = SubCatMagMSGFL.GetAt(buttonCat) as Message
				endif
				int buttonSubCat = SubCatMSG.Show()
				if buttonSubCat == 9
					Done = TRUE
				elseif buttonSubCat == 8
					;return to main menu
				else
					;Debug.Notification("Power menu")
					PowerMSG = PowerSubCatFL.GetAt(buttonSubCat) as Message
					PowerSP = SubCatSPFL.GetAt(buttonSubCat) as Spell
					int buttonPower = PowerMSG.Show(mslVTExpPoints.GetValueInt())
					if buttonPower == 3
						Done = TRUE
					elseif buttonPower == 2
						;return to main menu
					elseif buttonPower == 1
						;Debug.MessageBox("You do not have enough points available to unlock this power!")
						mslVTPSMenuNoPointsMSG.Show()
					elseif buttonPower == 0
						if LevelAFL.HasForm(PowerSP)
							mslVTExpPoints.Mod(-1)
							Game.GetPlayer().AddSpell(PowerSP)
						elseif LevelBFL.HasForm(PowerSP)
							mslVTExpPoints.Mod(-2)
							Game.GetPlayer().AddSpell(PowerSP)
						elseif LevelCFL.HasForm(PowerSP)
							mslVTExpPoints.Mod(-3)
							Game.GetPlayer().AddSpell(PowerSP)
						endif
						Done = TRUE
						;Debug.Notification("You have gained a new power!")
						mslVTPSMenuGainMSG.Show()
						PowerLearnedSM.Play(akTarget)
					endif
				endif
			endif
		endif
	endwhile

Endevent

Message Property MainMSG Auto
Formlist Property MainSPFL Auto
Formlist Property MainMSGFL Auto

Message Property CatMSG Auto
Formlist Property CatSPFL Auto

Message Property SubCatMSG Auto
Formlist Property SubCatSPFL Auto
Formlist Property SubCatSubMSGFL Auto
Formlist Property SubCatMigMSGFL Auto
Formlist Property SubCatMagMSGFL Auto

Formlist Property PowerMainFL Auto
Formlist Property PowerCatFL Auto
Formlist Property PowerSubCatFL Auto
Message Property PowerMSG Auto
Spell Property PowerSP Auto
Formlist Property LevelAFL Auto
Formlist Property LevelBFL Auto
Formlist Property LevelCFL Auto

GlobalVariable Property mslVTExpPoints Auto

Sound Property PowerLearnedSM Auto
Message Property mslVTPSMenuGainMSG Auto

Message Property mslVTPSMenuNoPointsMSG Auto