Scriptname mslVTPwBMRiteSCR extends ActiveMagicEffect  

import MSLVTVampire

Event OnEffectStart(Actor akTarget, Actor akCaster)
	Float UWMult = 1.0
	If akCaster.HasPerk(DLC1UnearthlyWill)
		UWMult = 0.67
	Endif
	CostTotal = CostBase*mslVTCostMultINS.GetValue()*UWMult
	if MSLVTVampire.getBlood(akCaster) < CostTotal
		;Debug.Notification("I do not have enough blood to perform this ritual!")
		RiteFailMSG.Show()
	else
		akCaster.PlayIdle(RiteIdle)
		RiteVisual.Play(akCaster)
		utility.wait(2.0)
		RiteSpell.Cast(akCaster, akTarget)
		MSLVTVampire.modBlood(akCaster, -CostTotal)
		mslVTExp.Mod(CostBase/2*mslVTExpMult.GetValue())
	endif
Endevent

Event OnEffectFinish(Actor akTarget, Actor akCaster)
	akCaster.RemoveSpell(RiteSpell)
Endevent

Idle Property RiteIdle  Auto  
;   
GlobalVariable Property mslVTCostMultINS  Auto  
GlobalVariable Property mslVTExp  Auto
GlobalVariable Property mslVTExpMult Auto
Spell Property RiteSpell Auto
VisualEffect Property RiteVisual Auto
Message Property RiteFailMSG Auto
Perk Property DLC1UnearthlyWill Auto
Float Property CostBase Auto
Float Property CostTotal Auto