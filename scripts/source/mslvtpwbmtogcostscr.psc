Scriptname mslVTPwBMTogCostSCR extends ActiveMagicEffect  

Event OnEffectStart(Actor akTarget, Actor akCaster)
	Float UWMult = 1.0
	If akTarget.HasPerk(DLC1UnearthlyWill)
		UWMult = 0.67
	Endif
	CostTotal = CostBase*UWMult
	MSLVTVampire.modDetAct(akCaster, CostTotal)
Endevent

Event OnEffectFinish(Actor akTarget, Actor akCaster)
	MSLVTVampire.modDetAct(akCaster, -CostTotal)
	If MSLVTVampire.getBlood(akCaster) <= 0
		;Debug.Notification("The power sparked and fizzled miserably. I am too hungry to maintain it right now!")
		PowerFailMSG.Show()
		akTarget.RemoveSpell(CurrentEffect)
	Endif
	If akTarget.HasSpell(ToggleEffect) == 0
		akTarget.RemoveSpell(CurrentEffect)
	Endif
Endevent


;GlobalVariable Property mslVTDetAct  Auto  
Spell Property CurrentEffect  Auto  
Spell Property ToggleEffect  Auto  
Message Property PowerFailMSG Auto
Perk Property DLC1UnearthlyWill Auto
Float Property CostBase Auto
Float Property CostTotal Auto