Scriptname mslVTPwInsCostSCR extends ActiveMagicEffect  
{IF blood points enough - allows effect to procees, consumes blood points, increases experience. Otherwise dispells the effect}
import MSLVTVampire

Event OnEffectStart(Actor akTarget, Actor akCaster)
	CostTotal = CostBase*mslVTCostMultINS.GetValue()
	If MSLVTVampire.getBlood(akCaster) < CostTotal
		;Debug.Notification("The power sparked and fizzled miserably. I am too hungry to maintain it right now!")
		PowerFailMSG.Show()
		akTarget.DispelSpell(CurrentEffect)
	else
		MSLVTVampire.modBlood(akCaster, -CostTotal)
		mslVTExp.Mod(CostBase/2*mslVTExpMult.GetValue())
		;Debug.Notification("Hit!")
	Endif
Endevent

; 
GlobalVariable Property mslVTCostMultINS  Auto  
GlobalVariable Property mslVTExp  Auto
GlobalVariable Property mslVTExpMult Auto
Spell Property CurrentEffect  Auto  
Message Property PowerFailMSG Auto
Float Property CostBase Auto
Float Property CostTotal Auto