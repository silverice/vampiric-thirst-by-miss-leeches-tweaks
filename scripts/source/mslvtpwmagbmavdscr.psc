Scriptname mslVTPwMagBMAVDSCR extends activemagiceffect  

float TargHealth

Event OnEffectStart(Actor akTarget, Actor akCaster)
	TargHealth = akTarget.GetAV("Health")
Endevent

Event OnEffectFinish(Actor akTarget, Actor akCaster)
	if akTarget.IsDead() == TRUE
		MSLVTVampire.modBlood(akCaster, TargHealth*mslVTBloodRatio.GetValue())
		mslVTExp.Mod((TargHealth*mslVTBloodRatio.GetValue())/5*mslVTExpMult.GetValue())
	endif
Endevent

GlobalVariable Property mslVTBloodRatio Auto
GlobalVariable Property mslVTExp Auto
GlobalVariable Property mslVTExpMult Auto