Scriptname mslVTPwMagBMBFOPSCR extends activemagiceffect  

actor Target

Event OnEffectStart(actor akTarget, actor akCaster)
	Target = akTarget
	Float UWMult = 1.0
	If akCaster.HasPerk(DLC1UnearthlyWill)
		UWMult = 0.67
	Endif
	CostTotal = CostBase*mslVTCostMultINS.GetValue()*UWMult
	RegisterForSingleUpdate(0.1)
Endevent

Event OnUpdate()
	If MSLVTVampire.getBlood(GetCasterActor()) <= 0
		Target.InterruptCast()
		FailStagger.Cast(Target, Target)
		RiteFailMSG.Show()
	Else
		Target.RestoreAV("magicka", CostBase/3)
		MSLVTVampire.modBlood(GetCasterActor(), -CostTotal)
		mslVTExp.Mod(CostBase/2*mslVTExpMult.GetValue())
		RegisterForSingleUpdate(0.1)
	Endif
Endevent

Event OnEffectFinish(actor akTarget, actor akCaster)
	UnregisterForUpdate()
Endevent

GlobalVariable Property mslVTBloodCur Auto
GlobalVariable Property mslVTCostMultINS  Auto  
GlobalVariable Property mslVTExp  Auto
GlobalVariable Property mslVTExpMult Auto
Message Property RiteFailMSG Auto
Perk Property DLC1UnearthlyWill Auto
Float Property CostBase Auto
Float Property CostTotal Auto
Spell Property FailStagger Auto