Scriptname mslVTPwMagShaARCSCR extends activemagiceffect  

Event OnEffectStart(Actor akTarget, Actor akCaster)
	if akTarget.GetItemCount(ClawsWPN) != 1
		akTarget.RemoveItem(ClawsWPN, akTarget.GetItemCount(ClawsWPN), true)
		akTarget.AddItem(ClawsWPN, 1, true)
	endif
	if akTarget.GetEquippedWeapon() != ClawsWPN && akTarget.GetEquippedItemType(1) == 0
		akTarget.EquipItem(ClawsWPN, false, true)
	endif
	RegisterForSingleUpdate(1)
Endevent

Event OnUpdate()
	if GetTargetActor().GetItemCount(ClawsWPN) != 1
		GetTargetActor().RemoveItem(ClawsWPN, GetTargetActor().GetItemCount(ClawsWPN), true)
		GetTargetActor().AddItem(ClawsWPN, 1, true)
	endif
	if GetTargetActor().GetEquippedWeapon() != ClawsWPN && GetTargetActor().GetEquippedItemType(1) == 0
		GetTargetActor().EquipItem(ClawsWPN, false, true)
	endif
	RegisterForSingleUpdate(1)
Endevent

Event OnEffectFinish(Actor akTarget, Actor akCaster)
	akTarget.RemoveItem(ClawsWPN, akTarget.GetItemCount(ClawsWPN), true)
Endevent

Weapon Property ClawsWPN Auto