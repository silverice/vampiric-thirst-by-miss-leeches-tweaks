Scriptname mslVTPwMagShaBMistSCR extends ActiveMagicEffect  

Event OnEffectStart(Actor akTarget, Actor akCaster)
	Float UWMult = 1.0
	If akCaster.HasPerk(DLC1UnearthlyWill)
		UWMult = 0.67
	Endif
	CostTotal = CostBase*mslVTCostMultINS.GetValue()*UWMult
	If MSLVTVampire.getBlood(akCaster) < CostTotal
		;Debug.Notification("The power sparked and fizzled miserably. I am too hungry to maintain it right now!")
		PowerFailMSG.Show()
		akTarget.DispelSpell(CurrentEffect)
	Else
		MSLVTVampire.modBlood(akCaster, -CostTotal)
		mslVTExp.Mod(CostBase/2*mslVTExpMult.GetValue())
		;Debug.Notification("Hit!")
		akCaster.ClearExtraArrows()
		OutEffect.Play(akTarget)
		MistFX.Play(akTarget, -1)
		utility.wait(2.0)
		OutEffect.Stop(akTarget)
		akTarget.AddSpell(MuffleSP, false)
		Game.DisablePlayerControls(false, true, false, false, false, false, true, false)
	Endif
Endevent

Event OnEffectFinish(Actor akTarget, Actor akCaster)
	akTarget.DispelSpell(CurrentEffect)
	InEffect.Play(akTarget)
	utility.wait(1.0)
	MistFX.Stop(akTarget)
	akTarget.RemoveSpell(MuffleSP)
	InEffect.Stop(akTarget)
	Game.EnablePlayerControls(false, true, false, false, false, false, true, false)
Endevent

VisualEffect Property MistFX Auto
VisualEffect Property InEffect Auto
VisualEffect Property OutEffect Auto
Spell Property MuffleSP Auto

GlobalVariable Property mslVTCostMultINS  Auto  
GlobalVariable Property mslVTExp  Auto
GlobalVariable Property mslVTExpMult Auto
Spell Property CurrentEffect  Auto  
Message Property PowerFailMSG Auto
Perk Property DLC1UnearthlyWill Auto
Float Property CostBase Auto
Float Property CostTotal Auto