Scriptname mslVTPwMagVSBSHControlSCR extends ActiveMagicEffect 

; It seems it unsummons, kills dead bodies 

Event OnEffectFinish(Actor akTarget, Actor akCaster)

;	Debug.Notification("Number of forms at the start: " + SHList.GetSize())

	CostTotal = CostBase*mslVTCostMultINS.GetValue()*SHList.GetSize()

	If MSLVTVampire.getBlood(akCaster) < CostTotal
		;Debug.Notification("I am too hungry to maintain so many undead thralls right now!")
		RiteFailMSG.Show()

		while (MSLVTVampire.getBlood(akCaster) < CostTotal) && SHList.GetSize() > 0
			Form frm = SHList.GetAt(0)
			Actor act = frm as Actor
			if act
				act.Kill()
			Endif

			SHList.RemoveAddedForm(frm)
			CostTotal = CostBase*mslVTCostMultINS.GetValue()*SHList.GetSize()
		endwhile

		MSLVTVampire.modBlood(akCaster, -CostTotal)
		mslVTExp.Mod(CostBase*SHList.GetSize()/2)
	else
		MSLVTVampire.modBlood(akCaster, -CostTotal)
		mslVTExp.Mod(CostBase*SHList.GetSize()/2)
	Endif

;	Debug.Notification("Number of forms after surplus termination: " + SHList.GetSize())


	SHList.Revert()


;	Debug.Notification("Number of forms at the end: " + SHList.GetSize())

Endevent

FormList Property SHList Auto

GlobalVariable Property mslVTCostMultINS  Auto  
GlobalVariable Property mslVTExp  Auto
Message Property RiteFailMSG Auto
Float Property CostBase Auto
Float Property CostTotal Auto