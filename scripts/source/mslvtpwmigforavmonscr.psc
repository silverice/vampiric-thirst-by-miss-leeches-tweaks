Scriptname mslVTPwMigForAVMOnSCR extends ActiveMagicEffect  

Event OnEffectStart(Actor akTarget, Actor akCaster)
	CostTotal = CostBase
	MSLVTVampire.modDetAct(akCaster, CostTotal)
Endevent

Event OnEffectFinish(Actor akTarget, Actor akCaster)
	MSLVTVampire.modDetAct(akCaster, -CostTotal)
	If MSLVTVampire.getBlood(akCaster) <= 0
		;Debug.Notification("The power sparked and fizzled miserably. I am too hungry to maintain it right now!")
		PowerFailMSG.Show()
		GetTargetActor().DispelSpell(CurrentEffect)
	Endif
	If GetTargetActor().HasSpell(ToggleEffect) == 0
		GetTargetActor().DispelSpell(CurrentEffect)
	Endif
Endevent


Spell Property CurrentEffect  Auto  
Spell Property ToggleEffect  Auto
Message Property PowerFailMSG Auto
;GlobalVariable Property mslVTDetAct  Auto
Float Property CostBase Auto
Float Property CostTotal Auto