Scriptname mslVTPwMigForAVMSCR extends activemagiceffect  

Event OnEffectStart(Actor Target, Actor Caster)
	CostTotal = CostBase
	MSLVTVampire.modDetAct(Caster, CostTotal)
EndEvent

Event OnEffectFinish(Actor Target, Actor Caster)
	MSLVTVampire.modDetAct(Caster, -CostTotal)
EndEvent

;
;GlobalVariable Property mslVTDetAct  Auto  
Float Property CostBase Auto
Float Property CostTotal Auto