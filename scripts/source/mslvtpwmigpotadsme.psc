Scriptname mslVTPwMigPotADSME extends ActiveMagicEffect  

Event OnEffectStart(Actor akTarget, Actor akCaster)
	JumpBon = mslVTAge.Value*35
	Game.SetGameSettingFloat("fJumpHeightMin", 76+JumpBon)
Endevent

Event OnEffectFinish(Actor akTarget, Actor akCaster)
	Game.SetGameSettingFloat("fJumpHeightMin", 76)
Endevent

Event OnPlayerLoadGame()
	JumpBon = mslVTAge.Value*35
	Game.SetGameSettingFloat("fJumpHeightMin",76+JumpBon)
EndEvent

GlobalVariable Property mslVTAge Auto
Float Property JumpBon Auto