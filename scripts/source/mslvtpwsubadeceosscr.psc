Scriptname mslVTPwSubADecEoSSCR extends activemagiceffect  

Event OnEffectStart(Actor Target, Actor Caster)
	;Debug.Notification("mslVTPwSubADecEoSSCR start")
	Target.SetAlpha (0.1, false)
EndEvent

Event OnEffectFinish(Actor Target, Actor Caster)
	;Debug.Notification("mslVTPwSubADecEoSSCR finit")
	OutroFX.apply()
	Utility.Wait(0.5)
	Target.SetAlpha (1.0, true)
	Target.RemoveSpell(CurrentEffect)
EndEvent

SPELL Property CurrentEffect  Auto  
SPELL Property ToggleEffect  Auto  
ImageSpaceModifier property OutroFX auto
