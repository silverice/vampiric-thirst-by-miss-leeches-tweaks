Scriptname mslVTPwSubDecAEoSTogSCR extends activemagiceffect  

Event OnEffectStart(Actor akTarget, Actor akCaster)

	;DEBUG.notification("has spell: " + akTarget.HasSpell(CurrentEffect))

	If akTarget.HasSpell(CurrentEffect) == 1
		mslVTPwTogOffMSG.Show()
		akTarget.RemoveSpell(CurrentEffect)
	Else
		If akTarget.IsInCombat()
			;Debug.Notification("I cannot use this power while enemies are aware of me!")
			FailNoCombatMSG.Show()
		Elseif MSLVTVampire.getBlood(akCaster) <= 0
			;Debug.Notification("The power sparked and fizzled miserably. I am too hungry to maintain it right now!")
			PowerFailMSG.Show()
		Else
			mslVTPwTogOnMSG.Show()
			IntroFX.apply()
			akTarget.SetAlpha (0.1, true)
			Utility.Wait(1.0)
			akTarget.AddSpell(CurrentEffect, false)
		Endif
	Endif
Endevent

SPELL Property CurrentEffect Auto
ImageSpaceModifier property IntroFX auto
Message Property FailNoCombatMSG Auto
Message Property PowerFailMSG Auto

Message Property mslVTPwTogOnMSG Auto
Message Property mslVTPwTogOffMSG Auto