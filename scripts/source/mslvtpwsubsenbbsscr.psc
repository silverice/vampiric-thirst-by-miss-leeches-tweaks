Scriptname mslVTPwSubSenBBSSCR extends activemagiceffect  

Event OnEffectStart(Actor akTarget, Actor akCaster)
	DetectLife.Cast(GetTargetActor())
	RegisterForSingleUpdate(4.8)
EndEvent

Event OnUpdate()
	DetectLife.Cast(GetTargetActor())
	RegisterForSingleUpdate(4.8)
EndEvent

Event OnEffectFinish(Actor akTarget, Actor akCaster)
	GetTargetActor().RemoveSpell(DetectLife)
EndEvent

SPELL Property DetectLife  Auto 