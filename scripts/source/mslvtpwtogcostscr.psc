Scriptname mslVTPwTogCostSCR extends activemagiceffect  

import MSLVTVampire

Event OnEffectStart(Actor akTarget, Actor akCaster)
	CostTotal = CostBase
	MSLVTVampire.modDetAct(akCaster, CostTotal)

	If MSLVTVampire.getBlood(akCaster) <= 0
		Dispel()
		return
	Endif

	; not used - faction condition used instead
	;RegisterForModEvent("mslVT_ChangeBlood", "OnBloodChange")
Endevent

Event OnBloodChange(form vampire, float bloodPoints)
	Actor akTarget = vampire as Actor
	If akTarget == GetTargetActor() && bloodPoints <= 0
		Dispel()
	Endif
EndEvent

Event OnEffectFinish(Actor akTarget, Actor akCaster)
	MSLVTVampire.modDetAct(akCaster, -CostTotal)
	If MSLVTVampire.getBlood(akCaster) <= 0
		;Debug.Notification("The power sparked and fizzled miserably. I am too hungry to maintain it right now!")
		PowerFailMSG.Show()
		akTarget.RemoveSpell(CurrentEffect)
	Endif
	If akTarget.HasSpell(ToggleEffect) == 0
		akTarget.RemoveSpell(CurrentEffect)
	Endif
Endevent

;GlobalVariable Property mslVTDetAct  Auto  
Spell Property CurrentEffect  Auto  
Spell Property ToggleEffect  Auto  
Message Property PowerFailMSG Auto
Float Property CostBase Auto
Float Property CostTotal Auto