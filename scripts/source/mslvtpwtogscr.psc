Scriptname mslVTPwTogSCR extends activemagiceffect  

Event OnEffectStart(Actor akTarget, Actor akCaster)
	If akTarget.HasSpell(CurrentEffect) == 1
		mslVTPwTogOffMSG.Show()
		akTarget.RemoveSpell(CurrentEffect)
	Else
		mslVTPwTogOnMSG.Show()
		akTarget.AddSpell(CurrentEffect, false)
	Endif
Endevent

SPELL Property CurrentEffect Auto

Message Property mslVTPwTogOnMSG Auto
Message Property mslVTPwTogOffMSG Auto