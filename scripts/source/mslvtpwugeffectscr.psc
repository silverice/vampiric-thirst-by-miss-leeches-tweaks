Scriptname mslVTPwUGEffectSCR extends activemagiceffect  

Event OnEffectStart(actor akTarget, actor akCaster)
	MSLVTVampire.setBloodMaxMod(akTarget, akTarget.GetLevel()*100)
Endevent

Event OnEffectFinish(actor akTarget, actor akCaster)
	MSLVTVampire.setBloodMaxMod(akTarget, 0)
Endevent
