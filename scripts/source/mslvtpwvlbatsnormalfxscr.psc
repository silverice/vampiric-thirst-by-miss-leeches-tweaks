Scriptname mslVTPwVLBatsNormalFXSCR extends ActiveMagicEffect  

Event OnEffectStart(Actor akTarget, Actor akCaster)

	OutEffect.Play(akTarget)
	BatsFX.Play(akTarget, -1)

Endevent

Event OnEffectFinish(Actor akTarget, Actor akCaster)

	akTarget.AddPerk(NoFallingDamage)

	;akTarget.SetAlpha(0, true)
	;utility.wait(1.0)
	;akTarget.SetAlpha(1, true)
	InEffect.Play(akTarget)
	OutEffect.Stop(akTarget)
	InEffect.Stop(akTarget)
	BatsFX.Stop(akTarget)

	utility.wait(3.0)
	akTarget.RemovePerk(NoFallingDamage)

Endevent

VisualEffect Property BatsFX Auto
VisualEffect Property InEffect Auto
VisualEffect Property OutEffect Auto

Perk Property NoFallingDamage Auto