Scriptname mslVTPwVLBatsNormalSCR extends ActiveMagicEffect  

import utility
import game
import debug

Actor Property Player Auto 
GlobalVariable Property CleanUp Auto

Event OnEffectStart(Actor akTarget, Actor akCaster)

	Float UWMult = 1.0
	If akCaster.HasPerk(DLC1UnearthlyWill)
		UWMult = 0.67
	Endif
	CostTotal = CostBase*mslVTCostMultINS.GetValue()*UWMult
	Player = akCaster

	If MSLVTVampire.getBlood(akCaster) < CostTotal
		;Debug.Notification("The power sparked and fizzled miserably. I am too hungry to maintain it right now!")
		PowerFailMSG.Show()
		MarkerCleanUp()
	elseif CleanUp.Value != 1 || Player.HasSpell(BatsFXSP)
		FindClosestReferenceOfTypeFromRef(myExplosion, Player, 10000.0).Delete()
	else
		MSLVTVampire.modBlood(akCaster, -CostTotal)
		mslVTExp.Mod(CostBase/2*mslVTExpMult.GetValue())
		Player.AddSpell(BatsFXSP, false)
		RegisterForSingleUpdate(0.01)
	Endif
	
EndEvent

Event OnUpdate()

	TransportArrow  = FindClosestReferenceOfTypeFromRef(myProjectile, Player, 10000.0)
	TransportTarget  = FindClosestReferenceOfTypeFromRef(myExplosion, Player, 10000.0)

	if (TransportArrow !=  None)
		RegisterForSingleUpdate(0.01)
	else
		if(TransportTarget != None)
			Player.SplineTranslateToRef(TransportTarget, 1000.0, 1800.0)
		endif
	endif

EndEvent

Event OnTranslationFailed()

	RegisterForSingleUpdate(0.01)

EndEvent

Event OnTranslationComplete()
	
	MarkerCleanUp()

	Player.RemoveSpell(BatsFXSP)

EndEvent

Event  OnEffectFinish(Actor akTarget, Actor akCaster)
	
	UnRegisterForUpdate()
	Game.GetPlayer().StopTranslation()

	MarkerCleanUp()

	Player.RemoveSpell(BatsFXSP)

EndEvent

Function MarkerCleanUp()

	if CleanUp.Value != 0
		CleanUp.Value = 0

		TransportTarget.Delete()
		FindClosestReferenceOfTypeFromRef(myExplosion, Player, 10000.0).Delete()
		FindClosestReferenceOfTypeFromRef(myExplosion, Player, 10000.0).Delete()
		FindClosestReferenceOfTypeFromRef(myExplosion, Player, 10000.0).Delete()
		TransportTarget  = None

		CleanUp.Value = 1
	endif

Endfunction

PROJECTILE Property MyProjectile  Auto 
Explosion Property myExplosion  Auto  
ObjectReference Property TransportTarget  Auto
ObjectReference Property TransportArrow  Auto  

SPELL Property BatsFXSP  Auto  


GlobalVariable Property mslVTCostMultINS  Auto  
GlobalVariable Property mslVTExp  Auto
GlobalVariable Property mslVTExpMult Auto
Message Property PowerFailMSG Auto
Perk Property DLC1UnearthlyWill Auto
Float Property CostBase Auto
Float Property CostTotal Auto