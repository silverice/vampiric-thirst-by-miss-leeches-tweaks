Scriptname mslVTPwVLGargoyleSCR extends activemagiceffect  

Event OnEffectStart(Actor akTarget, Actor akCaster)
	Float UWMult = 1.0
	If akTarget.HasPerk(DLC1UnearthlyWill)
		UWMult = 0.67
	Endif
	CostTotal = CostBase*mslVTCostMultINS.GetValue()*UWMult
	If MSLVTVampire.getBlood(akCaster) < CostTotal
		;Debug.Notification("The power sparked and fizzled miserably. I am too hungry to maintain it right now!")
		PowerFailMSG.Show()
		Self.Dispel()
	else
		MSLVTVampire.modBlood(akCaster, -CostTotal)
		mslVTExp.Mod(CostBase/2*mslVTExpMult.GetValue())
		;Debug.Notification("Hit!")
	Endif
Endevent

  
GlobalVariable Property mslVTCostMultINS  Auto  
GlobalVariable Property mslVTExp  Auto
GlobalVariable Property mslVTExpMult Auto
Message Property PowerFailMSG Auto
Perk Property DLC1UnearthlyWill Auto
Float Property CostBase Auto
Float Property CostTotal Auto