Scriptname mslVTPwVLVGCloakSCR extends ActiveMagicEffect  

Event OnEffectStart(Actor akTarget, Actor akCaster)
	float UWMult = 1.0
	If akCaster.HasPerk(DLC1UnearthlyWill)
		UWMult = 0.67
	Endif
	CostTotal = CostBase*UWMult
	MSLVTVampire.modDetAct(akCaster, CostTotal)
	;Debug.Notification("Vampiric Grip started! Deterioration: " + DetAct.Value)
EndEvent

Event OnEffectFinish(Actor akTarget, Actor akCaster)
	MSLVTVampire.modDetAct(akCaster, -CostTotal)
	;Debug.Notification("Vampiric Grip stopped! Deterioration: " + DetAct.Value)
EndEvent

;GlobalVariable Property mslVTDetAct  Auto
Float Property CostTotal  Auto  
Perk Property DLC1UnearthlyWill Auto
Float Property CostBase Auto
