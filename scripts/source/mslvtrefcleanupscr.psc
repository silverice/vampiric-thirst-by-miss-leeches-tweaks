Scriptname mslVTRefCleanupSCR extends ObjectReference  

{Basic script to remove objects on cell detach or cell reset.
- Martigen.}

EVENT OnInit()
    DeleteWhenAble()
endEvent

EVENT OnReset()
    DisableNoWait()
    Delete()
endEVENT