Scriptname mslVTSleepControlSCR extends activemagiceffect  

Event OnEffectStart(Actor akTarget, Actor akCaster)
	If (akTarget == Game.GetPlayer())
		RegisterForSleep()
	Endif
Endevent

Event OnSleepStart(float afSleepStartTime, float afDesiredSleepEndTime)

	MSLVTVampire.modDetActMult(GetTargetActor(), -0.5)
	MSLVTVampire.modDetNatMult(GetTargetActor(), -0.5)

	;Debug.Notification("Zzzzzz...")
Endevent

Event OnSleepStop(bool abInterrupted)
	utility.wait(2.0)
	MSLVTVampire.modDetActMult(GetTargetActor(), 0.5)
	MSLVTVampire.modDetNatMult(GetTargetActor(), 0.5)
	;Debug.Notification("Good morning!")
Endevent
