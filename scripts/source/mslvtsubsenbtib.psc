Scriptname mslVTSubSenBTiB extends ActiveMagicEffect  


Event OnEffectStart(Actor akTarget, Actor akCaster)

	string SexString
	string SexProString
	string SexPosString
	string RaceString 

	int MyRace = -1
	int MySex = -1

	;Check sex
	if MySex >= -1
		MySex = (akTarget.GetLeveledActorBase() as ActorBase).GetSex()
	endif

	if MySex == 0
		SexString = "male"
		SexProString = "He"
		SexPosString = "His"
	else
		SexString = "female"
		SexProString = "She"
		SexPosString = "Her"
	endif

	int HealthPerc = ((akTarget.GetAVPercentage("Health")*100) as INT)

	if akTarget.GetAVPercentage("Health") <= 0.10
		Debug.Notification(SexProString + " is standing at death's door and about to knock (Health: " + HealthPerc + "%)")
	elseif akTarget.GetAVPercentage("Health") <= 0.25
		Debug.Notification(SexPosString + " pulse is fading and life bleeding away (Health: " + HealthPerc + "%)")
	elseif akTarget.GetAVPercentage("Health") <= 0.50
		Debug.Notification(SexProString + " does not feel well at all (Health: " + HealthPerc + "%)")
	elseif akTarget.GetAVPercentage("Health") <= 0.75
		Debug.Notification(SexProString + " is unwell but not lethally (Health: " + HealthPerc + "%)")
	elseif akTarget.GetAVPercentage("Health") < 1.00
		Debug.Notification(SexProString + " feels a little unsound but is still in a good shape (Health: " + HealthPerc + "%)")
	elseif akTarget.GetAVPercentage("Health") == 1.00
		Debug.Notification(SexProString + " is in perfect health (Health: " + HealthPerc + "%)")
	Endif

	;Check race
	RaceString = akTarget.GetLeveledActorBase().GetRace().GetName()

	float Quality = akTarget.GetAV("Infamy")+100
	If Game.GetPlayer().HasMagicEffect(mslVTBloodAddictionME) && akTarget.GetAV("Infamy") < AddictionBar.Value
		Quality = (akTarget.GetAV("Infamy")+100)/2
	Endif

	if Quality <= 55
		Debug.Notification("The scent of this " + RaceString + " " + SexString + "'s blood could only make me sick! (Quality: " + (Quality as INT) + "%)")
	elseif Quality <= 65 && Quality > 55
		Debug.Notification("This " + RaceString + " " + SexString + " reeks of bad blood. (Quality: " + (Quality as INT) + "%)")
	elseif Quality <= 75 && Quality > 65
		Debug.Notification("I'm having trouble picturing myself drinking this " + RaceString + " " + SexString + "'s blood... (Quality: " + (Quality as INT) + "%)")
	elseif Quality <= 85 && Quality > 75
		Debug.Notification("The thought of drinking the blood of this" + RaceString + " " + SexString + " fillls me with distaste. (Quality: " + (Quality as INT) + "%)")
	elseif Quality <= 95 && Quality > 85
		Debug.Notification("The smell of this " + RaceString + " " + SexString + "'s blood is not very appealing. (Quality: " + (Quality as INT) + "%)")
	elseif Quality > 95 && Quality < 105
		Debug.Notification("The blood of this " + RaceString + " " + SexString + " has little distinctive flavour. (Quality: " + (Quality as INT) + "%)")
	elseif Quality >= 105 && Quality < 115
		Debug.Notification("This " + RaceString + " " + SexString + "'s blood does not smell too bad. (Quality: " + (Quality as INT) + "%)")
	elseif Quality >= 115 && Quality < 125
		Debug.Notification("The scent of this " + RaceString + " " + SexString + " has a pleasurable quality. (Quality: " + (Quality as INT) + "%)")
	elseif Quality >= 125 && Quality < 135
		Debug.Notification("I feel quite tempted to sink my fangs in this " + RaceString + " " + SexString + "'s neck... (Quality: " + (Quality as INT) + "%)")
	elseif Quality >= 135 && Quality < 145
		Debug.Notification("The blood of this " + RaceString + " " + SexString + " smells of... pleasure... (Quality: " + (Quality as INT) + "%)")
	elseif Quality >= 145
		Debug.Notification("I cannot help feeling drawn to this " + RaceString + " " + SexString + ". " + SexPosString + " flavour is like no other! (Quality: " + (Quality as INT) + "%)")
	endif

Endevent

GlobalVariable Property AddictionBar Auto
MagicEffect Property mslVTBloodAddictionME Auto