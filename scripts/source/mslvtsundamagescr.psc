Scriptname mslVTSunDamageSCR extends ActiveMagicEffect  

import mslVT_Utils

float _timeOld
float _previousDamage

float Function GameTimeDaysToRealTimeSeconds(float gametime)
	float gameSeconds = gametime * (60.0 * 60.0 * 24.0)
	return (gameSeconds / TimeScale.Value)
EndFunction

Event OnEffectStart(Actor akTarget, Actor akCaster)
	;logEffect(self, "start")
	_timeOld = GameDaysPassed.GetValue()
	RegisterForSingleUpdate(1)
Endevent

; Event OnEffectFinish(Actor akTarget, Actor akCaster)
; 	logEffect(self, "finish")
; EndEvent

Event OnUpdate()

	Actor akTarget = GetTargetActor()
	float gameHr = GameHour.GetValue()

	;If !akTarget.IsInInterior() && !akTarget.HasMagicEffectWithKeyword(SunDamageGrace) && \
		;gameHr <= mslVTSunDusk.GetValue() && gameHr >= mslVTSunDawn.GetValue() && \
		;!SunDamageExceptionWorldSpaces.HasForm(akTarget.GetWorldSpace()) && DLC1EclipseActive.GetValue() == 0 && mslVTSunDmg.GetValue() > 0

;Check time of day
		float TimeMult = fMax(0.0, gameHr * (0.5 - 0.02083333 * gameHr) - 2.0)

;Check weather type
		; int weatherClass = Weather.GetCurrentWeather().GetClassification()
		; If weatherClass == -1; None
		; 	WeatherMult = 0.0
		; Elseif weatherClass == 0; Pleasant
		; 	WeatherMult = 1.0`
		; Elseif weatherClass == 1; Cloudy
		; 	WeatherMult = 0.7
		; Elseif weatherClass == 2; Rainy
		; 	WeatherMult = 0.4
		; Elseif weatherClass == 3; Snowy
		; 	WeatherMult = 0.2
		; Endif
		float WeatherMult = 1.0

;Check hunger
		float HungerMod = MSLVTConfig.solveFlt(".hunger." + MSLVTVampire.getHungerStatus(akTarget) + ".sunDamageMod")

		; Lightness multiplier
		float lightness = akTarget.GetLightLevel() / 100.0
		; 'Fix' GetLightLevel value - as drawn weapon or spell increases it o_O
		if akTarget.IsWeaponDrawn()
			lightness -= 0.23
		endif
		; cubic curve
		; thanks to http://www.mycurvefit.com/
		float a=-4.000842
		float b=15.16944
		float c=-15.78847
		float d=5.602025
		; lightness/100      damage multiplier
		; 0		-4         
		; 0.44		0        > 0.44 - causes sun damage     
		; 0.5		0.6       
		; 0.75		0.8       
		; 1		1         
		; 2		8                
	 	lightness = fMax( 0.0, a + lightness * (b + lightness * (c + d  * lightness)) )
	 	; Lightness multiplier End

;Account for large time jumps and finally get burny!

		float TimeNew = GameDaysPassed.GetValue()
		float DelayMult = GameTimeDaysToRealTimeSeconds(TimeNew - _timeOld)
		_timeOld = TimeNew

		float maxSunDamage = (mslVTSunDmg.GetValue() + HungerMod + MSLVTVampire.getDetAct(akTarget)) * TimeMult * ;/WeatherMult */; DelayMult 
		float sunDamage = maxSunDamage * lightness ; lightness is the most dynamic factor

		; dempf, smooth incoming damage.
		float damageDiffPerc = Math.abs((sunDamage - _previousDamage) / sunDamage)
		if damageDiffPerc > 0.3
			sunDamage = _previousDamage + 0.3 * sunDamage
		endif
		_previousDamage = sunDamage
		; dempf end

		; Finally apply sun damage + effects
		if sunDamage > 0.0

			If !akTarget.IsSwimming() && !akTarget.IsDead()
				DmgVis1.Play(akTarget, 3)

				float damagePerc = sunDamage / akTarget.GetActorValue("Health")

				if damagePerc > 0.4
					DmgVis4.Play(akTarget, 3)
				Elseif damagePerc > 0.25
					DmgVis3.Play(akTarget, 3)
				Elseif damagePerc > 0.1
					DmgVis2.Play(akTarget, 3)
				endif
			Endif

			akTarget.damageAV("Health", sunDamage)
		endif

		;logEffect(self, "Lightness " + (akTarget.GetLightLevel() / 100.0) + " damage " + sunDamage)
		RegisterForSingleUpdate(1)

	; Else

	; 	_timeOld = 0
	; 	UnregisterForUpdate()

	; Endif

Endevent

GlobalVariable Property mslVTSunDmg Auto

GlobalVariable Property mslVTSunDawn Auto
GlobalVariable Property mslVTSunDusk Auto

GlobalVariable Property GameDaysPassed Auto
GlobalVariable Property GameHour Auto
GlobalVariable Property TimeScale Auto

Keyword Property SunDamageGrace Auto
WorldSpace Property Sovngarde Auto
FormList Property SunDamageExceptionWorldSpaces Auto
GlobalVariable Property DLC1EclipseActive Auto

SPELL Property HungerState2 Auto 
SPELL Property HungerState3 Auto  
SPELL Property HungerState4 Auto  
SPELL Property HungerState5 Auto  
EffectShader Property DmgVis1  Auto  
EffectShader Property DmgVis2  Auto  
EffectShader Property DmgVis3  Auto  
EffectShader Property DmgVis4  Auto

MagicEffect Property mslVTPwComBMForBloodShroudME Auto
