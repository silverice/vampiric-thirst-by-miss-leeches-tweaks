;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 6
Scriptname mslVTTIF__010187BE Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_5
Function Fragment_5(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
Actor akCaster = Game.GetPlayer()
CostTotal = CostBase*mslVTCostMultINS.GetValue()
If MSLVTVampire.getBlood(akCaster) < CostTotal
	;Debug.Notification("The power sparked and fizzled miserably. I am too hungry to maintain it right now!")
	PowerFailMSG.Show()
else
	MSLVTVampire.modBlood(akCaster, -CostTotal)
	mslVTExp.Mod(CostBase/2)
	(akSpeaker as Actor).AddToFaction(EntranceFaction)
Endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

SPELL Property EntrancementSP  Auto   

  

GlobalVariable Property Cost  Auto  

GlobalVariable Property mslVTCostMultINS  Auto  

GlobalVariable Property mslVTExp  Auto  

Message Property PowerFailMSG Auto

Float Property CostBase Auto

Float Property CostTotal Auto

Faction Property EntranceFaction  Auto  
