;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 2
Scriptname mslVTTIF__030E1116 Extends TopicInfo Hidden

;BEGIN FRAGMENT Fragment_1
Function Fragment_1(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
;Debug.MessageBox("THERE IS ONLY PIE")
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akSpeakerRef)
Actor akSpeaker = akSpeakerRef as Actor
;BEGIN CODE
;Debug.MessageBox("THE CAKE IS A LIE")
akSpeaker.SetFactionRank(mslVTFeedDialogueFailFAC, Game.GetPlayer().GetBaseActorValue("Speechcraft") as Int)

if (akSpeaker.GetRelationshipRank(Game.GetPlayer()) < 1) || (akSpeaker.GetRelationshipRank(Game.GetPlayer()) == 1 && akSpeaker.GetFactionRank(mslVTMasqPlayerKnownFac) < 0) || (akSpeaker.GetRelationshipRank(Game.GetPlayer()) < 0 && akSpeaker.GetFactionRank(mslVTMasqPlayerKnownFac) > 0)
Game.SendWereWolfTransformation()
endif

akSpeaker.SetFactionRank(mslVTMasqPlayerKnownFac, -1)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Faction Property mslVTFeedDialogueFailFAC  Auto  

Faction Property mslVTMasqPlayerKnownFac  Auto  
