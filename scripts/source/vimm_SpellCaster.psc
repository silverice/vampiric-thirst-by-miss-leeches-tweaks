Scriptname vimm_SpellCaster extends activemagiceffect  
{Casts specified spell on specified target}

import mslVT_Utils

Spell Property castSpell Auto
Int Property castChance = 101 Auto
Int Property targeting = 0x01 Auto
Float Property castDelay = 0.0 Auto
Bool Property castOnEffectStart = True Auto

String Property _debugName Auto

Event OnEffectStart(Actor akTarget, Actor akCaster)
	; For debug purposes only!!!
	; Remember base effect, so in OnEffectFinish we still can print effect name
	; This is a workaround as GetBaseObject() in OnEffectFinish fails
	if self._debugName == ""
		self._debugName = GetBaseObject().GetName()
	endif

	if self.castOnEffectStart
		self.proceed(akTarget, akCaster)
	endif
EndEvent

Event OnEffectFinish(Actor akTarget, Actor akCaster)
	if self.castOnEffectStart == False
		self.proceed(akTarget, akCaster)
	endif
EndEvent

string function _describeProcess(Actor akTarget, Actor akCaster)
	string s = "[" + self._debugName + "]"

	string spl = "None"
	if self.castSpell != None
		spl = self.castSpell.GetName()
	endif

	int ttt = self.targeting
	if ttt == 1
		s += "c " + akCaster + " spl " + spl + " on t " + akTarget
	elseif ttt == 10
		s += "t " + akTarget + " spl " + spl + " on c" + akCaster
	elseif ttt == 0
		s += "c " + akCaster + " spl " + spl + " on self"
	else;if ttt == 11
		s += "t " + akTarget + " spl " + spl + " on self"
	endif
	return s
endfunction

function proceed(Actor akTarget, Actor akCaster)

	if Utility.RandomInt() < self.castChance
		int ttt = self.targeting

		if self.castDelay != 0.0
			Utility.Wait(self.castDelay)	; no sense to pin thread if cast chance is too low OR castDelay is zero
		endif

		log(_describeProcess(akTarget, akCaster))

		if ttt == 1
			log("[" + self._debugName + "] c " + akCaster + " spl " + self.castSpell + " on t " + akTarget)
			self.castSpell.Cast(akCaster, akTarget)
		elseif ttt == 10
			log("[" + self._debugName + "] t " + akTarget + " spl " + self.castSpell + " on c" + akCaster)
			self.castSpell.Cast(akTarget, akCaster)
		elseif ttt == 0
			log("[" + self._debugName + "] c " + akCaster + " spl " + self.castSpell + " on self")
			self.castSpell.Cast(akCaster, akCaster)
		else;if ttt == 11
			log("[" + self._debugName + "] t " + akTarget + " spl " + self.castSpell + " on self")
			self.castSpell.Cast(akTarget, akTarget)
		endif
	else
		log(_describeProcess(akTarget, akCaster) + ". Too low chance to proceed")
	endif
endfunction
