Scriptname vimm_SpellDebugger extends activemagiceffect  
{Casts specified spell on specified target}

import mslVT_Utils

String Property _debugName Auto

Event OnEffectStart(Actor akTarget, Actor akCaster)
	; For debug purposes only!!!
	; Remember base effect, so in OnEffectFinish we still can print effect name
	; This is a workaround as GetBaseObject() in OnEffectFinish fails
	if self._debugName == ""
		self._debugName = GetBaseObject().GetName()
	endif

	log("[" + self._debugName + "] started with caster " + akCaster + " target " + akTarget)
EndEvent

Event OnEffectFinish(Actor akTarget, Actor akCaster)
	log("[" + self._debugName + "] finished with caster " + akCaster + " target " + akTarget)
EndEvent
